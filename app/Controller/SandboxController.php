<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class SandboxController extends AppController
{
 
  	function index()
  	{
     
  	}
  	function authentication()
	{
	        
	}
  	function api_parameter()
    {
        
    }
    function api_services()
    {
        
    }
    function rc()
    {
        
    }
    function data_dummy()
    {
        
    }
    function daftar_produk()
    {
        $this->loadModel('Product');
        $daftar_produk = [];
        if(!empty($this->request->data['produk'])){
            if ($this->request->data['produk']=='100') {
                $daftar_produk = $this->Product->query("select distinct on (id) * from products p where 
                    id = '80'
                    or id = '100'
                    or id = '105'
             ");
            }
            else{
            $daftar_produk = $this->Product->query("select distinct on (id) * from products p where
                product_category_id = '".$this->request->data['produk']."'
             "); 
            }   
        }
        $this->set("products", $daftar_produk);
    }
    public function produk($id)
    {
        $this->loadModel('Product');
        $id=$this->request->data['produk'];
        echo $id;
        $daftar_produk = $this->Product->query("select distinct on (id) * from products p where id = '".$id."' ");
        print_r($daftar_produk);
        die();
    }
    function test_api()
    {
        
    }
    public function test_api_response()
    {
        $this->layout = "ajax";
        $this->render(false);

        if ($this->request->data['trx_type'] =='2200') {
            $respon["trx_id"] = "6308791542";
            $respon["stan"] = "351977";
            $respon["harga"] = "60000.0";
            $respon["saldo"] = "999996282240156";
            $respon["datetime"] = "20200122110614";
            $respon["merchant_code"] = "6021";
            $respon["bank_code"] = "4510017";
            $respon["rc"] = "0000";
            $respon["terminal_id"] = "0000000000000048";
            $respon["material_number"] = "12345678901";
            $respon["subscriber_id"] = "100456789071";
            $respon["pln_refno"] = "                                ";
            $respon["switcher_refno"] = "044121C9BB8D7A44F8FF11204F63CCFE";
            $respon["subscriber_name"] = "SUBSCRIBER NAME&         ";
            $respon["subscriber_segmentation"] = "R1  ";
            $respon["power"] = "1300";
            $respon["admin_charge"] = "0";
            $respon["distribution_code"] = "  ";
            $respon["service_unit"] = "     ";
            $respon["service_unit_phone"] = "000000000000015";
            $respon["max_kwh_unit"] = "";
            $respon["total_repeat"] = "0";
            $respon["token"] = "12312312311231231231";
            $respon["amount"] = "20000";
            $respon["angsuran"] = ".00";
            $respon["power_purchase"] = "20000.00";
            $respon["jml_kwh"] = "12.8";
            $respon["ppn"] = "0.00";
            $respon["ppj"] = "0.00";
            $respon["meterai"] = "0.00";
            $respon["power_purchase_unsold"] = "";
            $respon["power_purchase_unsold2"] = "";
            $respon["bit11"] = "";
            $respon["bit12"] = "";
            $respon["bit48"] = "SWID003123456789011004567890711                                044121C9BB8D7A44F8FF11204F63CCFE        SUBSCRIBER NAME&         R1       13000200000000002000000000020000000000200000000002000000000020000020000001000000012812312312311231231231";
            $respon["bit62"] = "       00000000000001500000000000000000";
            $respon["info_text"] = "RINCIAN TAGIHAN DAPAT DIAKSES& DI \"www.pln.co.id&\" ATAU PLN TERDEKAT";
        }
        else{
            $respon['trx_id'] = $this->request->data['trx_id'];
            $respon['stan'] = "000000351964";
            $respon['datetime'] = $this->request->data['trx_date'];
            $respon['merchant_code'] = "6021";
            $respon['bank_code'] = "4510017";
            $respon['rc'] = "0000";
            $respon['terminal_id'] = "0000000000000048";
            $respon['material_number'] = $this->request->data['cust_msisdn'];
            $respon['subscriber_id'] = $this->request->data['cust_account_no'];
            $respon['pln_refno'] = "";
            $respon['switcher_refno'] = "";
            $respon['subscriber_name'] = "SUBSCRIBER NAME&         ";
            $respon['subscriber_segmentation'] = "  R1";
            $respon['power'] = "1300";
            $respon['admin_charge'] = "0";
            $respon['distribution_code'] = "";
            $respon['service_unit'] = "";
            $respon['service_unit_phone'] = "";
            $respon['max_kwh_unit'] = "00000";
            $respon['total_repeat'] = "0";
            $respon['power_purchase_unsold'] = "";
            $respon['power_purchase_unsold2'] = "";
            $respon['saldo'] = "";
            $respon['bit11'] = "";
            $respon['bit12'] = "";
            $respon['bit48'] = "SWID003123456789011004567890711                                                                SUBSCRIBER NAME&           R100000130000000000000";
            $respon['bit62'] = "                      00000000000000000";
        }
        $trx= array('trx'=>$respon);
        $data= array('data'=>$trx);

        echo json_encode($data, JSON_PRETTY_PRINT);
        
    }
    function download()
    {
        
    }
    function contact()
    {
        
    }

}