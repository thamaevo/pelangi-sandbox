<?php
App::import('vendor', 'Spreadsheet_Excel_Writer', array('file' => '../vendors/Pear/Spreadsheet/Excel/Writer.php')); 
 App::import('Vendor','',array('file' => 'OLE/PPS/Root.php'));
App::import('Vendor','',array('file' => 'OLE/PPS/File.php'));
App::uses('AppController', 'Controller');
/**
 * Transactions Controller
 *
 * @property Transaction $Transaction
 * @property PaginatorComponent $Paginator
 */
class TransactionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
        
        function getRandomLocation() {

            $min = 1;
            $max = 6;
            $quantity = 3;
            $numbers = range($min, $max);
            shuffle($numbers);
            return array_slice($numbers, 0, $quantity);

        } 
        public function DisplayDownline() {
            $this->loadModel("Mutation");
            $users = $this->Mutation->User->find('list',array(
                'conditions'=>array("or"=>array(
                    'User.upline'=> $this->Session->read("Auth.User.id"),
                    //'User.username like'=>"%".substr($this->Session->read('Auth.User.username'), 0, 3)."%' and User.group_id=3"
                    )),
                'fields'=>array('User.id','User.username'),
                'order'=>array('User.username')
                //'limit'=> '20'
            ));
            $this->set(compact('users'));
            //$this->set('saldo',MutationsController::getLastBalance($this->Session->read('Auth.User.id')));
            $saldo = $this->Mutation->find('first', array(
                    'conditions' => array('Mutation.user_id' => $this->Session->read("Auth.User.id")),
                    'fields' => array('sum(amount) as balance')
            ));
            $this->set('saldo', $saldo);
            
            $this->loadModel("Users");
            $result1 = $this->Users->query("select username from users where id=".$this->Session->read("Auth.User.id").";");
            $down1 = $this->Users->query("select username,id from users where upline=".$this->Session->read("Auth.User.id").";");            

            $output="<table class='dwn' id='mytable'><tr id='title'><th>Username</th><th>Saldo</th></tr>";
            $output=$output."<tr data-depth='0' class='collapse level0' id='data'><td>".$result1[0][0]['username']."</td><td>Rp. ".$saldo[0][0]['balance']."</td>";
            $output=$output."</tr>";

            $i=0;
            $output=$output."<tr data-depth='1' class='collapse level1' id='data'>";
            foreach ($down1 as $downs):
                $balance = $this->Mutation->find('first', array(
                    'conditions' => array('Mutation.user_id' => $down1[$i][0]['id']),
                    'fields' => array('sum(amount) as balance')
                ));
                $this->set('balance', $balance[0]['balance']);
                
                if ($balance[0]['balance'] == null) {
                    $saldodl=0;
                } else
                    $saldodl=$balance[0]['balance'];
                    $output=$output."<td>".$down1[$i][0]['username']."</td><td>Rp. ".$saldodl."</td>";
                    $araynya=$this->NextDownline($down1[$i][0]['id'],3,$i);
                    $output=$output.$araynya['user'];
                    //$output=$output."</tr>";                    
                    $i++;           
            endforeach;
                $output=$output."</tr></table>";
                $this->set('output',$output);
        }
        
        public function NextDownline($user_id=null,$limit,$reveal){
            //$this->layout= 'ajax';
            $this->loadModel("Mutation"); 
            $this->loadModel("Users");
            $output['user']="";
            $limit = $limit-1;
            if ($limit==0) {
            
            } else {
                if (!empty($user_id)) {
                    $downline = $this->Users->query("select username,id from users where upline=".$user_id.";");
                    $output['user']=$output['user']."<tr data-depth='2' class='collapse level2' id='data'>";
                    $i=0;
                    $revkirim = (string)$reveal.(string)$i;
                foreach ($downline as $downlines):       
                    $balance = $this->Mutation->find('first', array(
                        'conditions' => array('Mutation.user_id' => $downline[$i][0]['id']),
                        'fields' => array('sum(amount) as balance')
                    ));
                    $this->set('balance', $balance[$i][0]['balance']);
                    $output['user']=$output['user']."<td>".$downline[$i][0]['username']."</td><td>Rp. ".$balance[$i][0]['balance']."</td>";
                    $araynya = $this->NextDownline($downline[$i][0]['id'],$limit,$revkirim);
                    $output['user']=$output['user'].$araynya['user'];
                    //$output['user']=$output['user']."</tr>";
                    $i++;
                endforeach;
                    $output['user']=$output['user']."</tr>";
                }
            }
        
            return $output;
        }
		
		
        public function services() {
	$this->layout = 'ajax';
	$this->Transaction->recursive = 0;
		$this->set('transactions', $this->Paginator->paginate());
                
				
				
				$this->loadModel("Product_categories");
                $product_categories_game = $this->Product_categories->find('all',array(
				'conditions'=>array('Product_categories.category_id'=>2),
                    'fields'=> array('Product_categories.id','Product_categories.name','Product_categories.category_id'),
                    'order' => array('Product_categories.name')
					
                    ));
                $this->set(compact('product_categories_game'));
                
				$product_categories_pul = $this->Product_categories->find('all',array(
				'conditions'=>array('Product_categories.category_id'=>3),
                    'fields'=> array('Product_categories.id','Product_categories.name','Product_categories.category_id'),
                    'order' => array('Product_categories.name')
					
                    ));
                $this->set(compact('product_categories_pul'));
                
				
               ////if(strpos($this->Session->read("Auth.User.username"),"sca02")!==FALSE || strpos($this->Session->read("Auth.User.username"),"sca01")!==FALSE || strpos($this->Session->read("Auth.User.username"),"pid029")!==FALSE){
					//if(strpos($this->Session->read("Auth.User.username"),"sca02")!==FALSE || strpos($this->Session->read("Auth.User.username"),"sca01")!==FALSE){
				/* 	if(strpos($this->Session->read("Auth.User.username"),"sca01")!==FALSE || strpos($this->Session->read("Auth.User.username"),"sca02")!==FALSE){
				////if(substr($this->Session->read("Auth.User.username"),5)=="sca02" || substr($this->Session->read("Auth.User.username"),5)=="sca01" || substr($this->Session->read("Auth.User.username"),6)=="pid029"){
                $this->loadModel("Products");
                $products = $this->Products->find('all',array(
                    'fields'=> array('Products.id','Products.code','Products.product_category_id','Products.description'),
                    'order' => array('Products.code'),
                    'conditions' => array('Products.id'=> array('354','355','356'))
					));
                $this->set(compact('products'));
			}else{ */
			 $this->loadModel("Products");
                $products = $this->Products->find('all',array(
                    'fields'=> array('Products.id','Products.code','Products.product_category_id','Products.description'),
                    'order' => array('Products.code')
					));
                $this->set(compact('products'));
			//}
				
				

            //$this->set(compact('products'));
            $this->set('productbycategory',$productbycategory);

                $this->set('main_server',Configure::read('main.server'));
                
                $this->loadModel("Kolektifs");
                $kolektif = $this->Kolektifs->find('all',array(
                    'conditions' => array('Kolektifs.user_id'=> $this->Session->read("Auth.User.id"))
                ));
                $this->set('kolektif_list',$kolektif);
	
				$this->set('nama_depan',$this->Session->read("Auth.User.first_name"));
				$this->set('nama_blkng',$this->Session->read("Auth.User.last_name"));
                                
                $empty_position = $this->getRandomLocation();
      
                $this->set('emptiness',$empty_position);                
                
	}
        
        public function kolektif() {
            
        }

	public function administrasi() {
            $this->layout = 'administrasi';
        }
        
		 public function downline_report() {
            $this->layout = "ajax";
        }
		      
        public function report() {
            $this->layout = "ajax";
            // echo "test date: ".date('Y-m-d');
        }
        
		             public function reporting($id=null, $date_start=null, $date_end=null) {
            $this->layout = 'ajax';

            $tgl_start = date('Y-m-d');
            $tgl_end = date('Y-m-d');
            // echo "tgl start : ".$tgl_start;
            // echo "<br/> formater : ".date('m-d-Y', strtotime($tgl_start));

            if (!empty($_POST['date_start'])) {
                $tgl_start = $_POST['date_start'];
            }
            if (!empty($_POST['date_end'])) {
                $tgl_end = $_POST['date_end'];
            }

            $this->set('tgl_start',$tgl_start);
            $this->set('tgl_end',$tgl_end);

            $conditions = array();
            $conditions['Mutation.user_id'] = $this->Session->read('Auth.User.id');

            // $conditions2 = array();
            // $conditions2['Mutation.user_id'] = $this->Session->read('Auth.User.id');

            if (!isset($tgl_start)) {
                // echo "masuk if";
                $conditions['Mutation.create_date >='] = date("m/d/Y")." 00:00:00";
                // $conditions['Mutation.create_date >='] = date("Y-m-d")." 00:00:00";
                // $conditions2['Mutation.create_date >='] = date("m/d/Y")." 00:00:00";
            }  else {
                // echo "masuk else ".$tgl_start;
                $conditions['Mutation.create_date >='] = $tgl_start." 00:00:00";
                // $conditions2['Mutation.create_date >='] = $tgl_start." 00:00:00";
            }

            if (!isset($tgl_end)) {
                $conditions['Mutation.create_date <='] = date("m/d/Y")." 23:59:59";
                // $conditions['Mutation.create_date <='] = date("Y-m-d")." 23:59:59";
                // $conditions2['Mutation.create_date <='] = date("m/d/Y")." 23:59:59";
            }  else {
                $conditions['Mutation.create_date <='] = $tgl_end." 23:59:59";
                // $conditions2['Mutation.create_date <='] = $tgl_end." 23:59:59";
            }
            if (date('Y-m-d', strtotime($tgl_start)) < date('Y-m-d')) {
                $this->Transaction->setDataSource('report'); 
            }
            $this->Transaction->recursive = -1;
            $transactions = $this->Transaction->find('all', array(
				// 'limit' => 1,
                'conditions'=> $conditions,
                'order' => array('Mutation.create_date asc'),
                'joins' => array(
                    array(
                        'table' => 'mutations',
                        'alias' => 'Mutation',
                        'type' => 'RIGHT',
                        'conditions' => array('Mutation.inbox_id = Transaction.inbox_id')
                    ),
                    array(
                        'table' => 'inboxes',
                        'alias' => 'Inbox',
                        'type' => 'LEFT',
                        'conditions' => array('Mutation.inbox_id = Inbox.id')
                    ),
                    array(
                        'table' => 'response_stats',
                        'alias' => 'ResponseStat',
                        'type' => 'LEFT',
                        'conditions' => array('cast(Inbox.status as text) = ResponseStat.code')
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array('Transaction.product_id = Product.id')
                    ),
				    array(
                        'table' => 'pln_postpaids',
                        'alias' => 'PLNPostpaid',
                        'type' => 'LEFT',
                        'conditions' => array('PLNPostpaid.inbox_id = Inbox.id')
                    ),
				    array(
                        'table' => 'telkoms',
                        'alias' => 'Telkom',
                        'type' => 'LEFT',
                        'conditions' => array('Transaction.inbox_id = Telkom.inbox_id')
                    ),
                ),
                'fields' => array(
                    'Mutation.id',
                    'Mutation.create_date',
                    'Mutation.note',
                    'Product.nama_display',
                    'Inbox.nominal',
                    'Inbox.msisdn',
                    'Inbox.idpel',
                    'Mutation.amount',
                    'Mutation.balance',
                    'Inbox.status',
                    'ResponseStat.name',
                    'Product.id',
                    'Product.name',
                    'Product.nominal_display',
                    'Product.product_category_id',
                    'Transaction.inbox_id',
                    'Transaction.price_buy',
                    'Transaction.price_sell',
                    'count("Transaction"."id")',
                    'sum(cast("PLNPostpaid"."jumlahrek" as integer)) as lembar ',
                    'sum(cast("Telkom"."admin" as integer)/3000) as lembar_telkom'
                ),
                'group' => array(
                    'Mutation.id',
                    'Mutation.create_date',
                    'Mutation.note',
                    'Product.nama_display',
                    'Inbox.nominal',
                    'Inbox.msisdn',
                    'Inbox.idpel',
                    'Mutation.amount',
                    'Mutation.balance',
                    'Inbox.status',
                    'ResponseStat.name',
                    'Product.id',
                    'Product.name',
                    'Product.nominal_display',
                    'Product.product_category_id',
                    'Transaction.inbox_id',
                    'Transaction.price_buy',
                    'Transaction.price_sell'
                ),
            ));
            // debug($transactions);
            $this->set('transactions', $transactions);

           

            if ($id =='export'){
                
            
         /*       $tgl_start = date('Y-m-d');
                $tgl_end = date('Y-m-d');

                if (isset($_POST['date_start'])) {
                    $tgl_start = $_POST['date_start'];
                }
                if (isset($_POST['date_end'])) {
                    $tgl_end = $_POST['date_end'];
                }

                $this->set('tgl_start',$tgl_start);
                $this->set('tgl_end',$tgl_end);*/

                $a = $_GET['date_start'];
                $b = $_GET['date_end'];

                $conditions = array();
                $conditions['Mutation.user_id'] = $this->Session->read('Auth.User.id');

                if ($a) {
                    $conditions['Mutation.create_date >='] = date("Y-m-d",strtotime($a))." 00:00:00";
                    // $conditions2['Mutation.create_date >='] = date("m/d/Y")." 00:00:00";
                }  else {
                    $conditions['Mutation.create_date >='] = date("Y-m-d")." 00:00:00";
                    // $conditions2['Mutation.create_date >='] = $tgl_start." 00:00:00";
                }

                if ($b) {
                    $conditions['Mutation.create_date <='] = date("Y-m-d",strtotime($b))." 23:59:59";
                    // $conditions2['Mutation.create_date <='] = date("m/d/Y")." 23:59:59";
                }  else {
                    
                    $conditions['Mutation.create_date <='] = date("Y-m-d")." 23:59:59";
                    // $conditions2['Mutation.create_date <='] = $tgl_end." 23:59:59";
                }
                if (date('Y-m-d', strtotime($a)) < date('Y-m-d')) {
                $this->Transaction->setDataSource('report'); 
            }
            $this->Transaction->recursive = -1;
            $transactions = $this->Transaction->find('all', array(
                // 'limit' => 1,
                'conditions'=> $conditions,
                'order' => array('Mutation.create_date asc'),
                'joins' => array(
                    array(
                        'table' => 'mutations',
                        'alias' => 'Mutation',
                        'type' => 'RIGHT',
                        'conditions' => array('Mutation.inbox_id = Transaction.inbox_id')
                    ),
                    array(
                        'table' => 'inboxes',
                        'alias' => 'Inbox',
                        'type' => 'LEFT',
                        'conditions' => array('Mutation.inbox_id = Inbox.id')
                    ),
                    array(
                        'table' => 'response_stats',
                        'alias' => 'ResponseStat',
                        'type' => 'LEFT',
                        'conditions' => array('cast(Inbox.status as text) = ResponseStat.code')
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array('Transaction.product_id = Product.id')
                    ),
                    array(
                        'table' => 'pln_postpaids',
                        'alias' => 'PLNPostpaid',
                        'type' => 'LEFT',
                        'conditions' => array('PLNPostpaid.inbox_id = Inbox.id')
                    ),
                    array(
                        'table' => 'telkoms',
                        'alias' => 'Telkom',
                        'type' => 'LEFT',
                        'conditions' => array('Transaction.inbox_id = Telkom.inbox_id')
                    ),
                ),
                'fields' => array(
                    'Mutation.id',
                    'Mutation.create_date',
                    'Mutation.note',
                    'Product.nama_display',
                    'Inbox.nominal',
                    'Inbox.msisdn',
                    'Inbox.idpel',
                    'Mutation.amount',
                    'Mutation.balance',
                    'Inbox.status',
                    'ResponseStat.name',
                    'Product.id',
                    'Product.name',
                    'Product.nominal_display',
                    'Product.product_category_id',
                    'Transaction.inbox_id',
                    'Transaction.price_buy',
                    'Transaction.price_sell',
                    'count("Transaction"."id")',
                    'sum(cast("PLNPostpaid"."jumlahrek" as integer)) as lembar ',
                    'sum(cast("Telkom"."admin" as integer)/3000) as lembar_telkom'
                ),
                'group' => array(
                    'Mutation.id',
                    'Mutation.create_date',
                    'Mutation.note',
                    'Product.nama_display',
                    'Inbox.nominal',
                    'Inbox.msisdn',
                    'Inbox.idpel',
                    'Mutation.amount',
                    'Mutation.balance',
                    'Inbox.status',
                    'ResponseStat.name',
                    'Product.id',
                    'Product.name',
                    'Product.nominal_display',
                    'Product.product_category_id',
                    'Transaction.inbox_id',
                    'Transaction.price_buy',
                    'Transaction.price_sell'
                ),
            ));
        
                            
                // Set up the header array
                $titles = array(
                    'ID Trx' => 25,
                    'Waktu' => 25,
                    'Keterangan' => 25,
                    'Lembar' => 25,
                    'Nominal' => 25,
                    'Tujuan/IDpel' => 25,
                    'D' => 25,
                    'K' => 25,
                    'Saldo' => 25,
                    'Saldo Akhir' => 25,
                    'Status' => 25
                    
                );
                
                $rn = 0; // row number  
                // Build the XLS file using PEAR
                $xlsBook = new Spreadsheet_Excel_Writer();  
                $xlsBook->send("Laporan.xls");
                $xls =& $xlsBook->addWorksheet('Laporan');
                $xls->setLandscape();
				
				 $xlsBook->setVersion(8);
				

                /* Create styles for the spreadsheet */
                $format_bold =& $xlsBook->addFormat();
                $format_bold->setBold();
                $formatCurr =& $xlsBook->addFormat(array('Size' => 11));
                $formatCurr->setNumFormat('0');

                $date_format =& $xlsBook->addFormat();
                $date_format->setNumFormat('D/M/YYYY h:mm:ss');
                
                $main =& $xlsBook->addFormat(
                    array('Size' => 14,
                        'Align' => 'center',
                        'Color' => 'black',
                        'Bold' => 'true'
                    ));
                $main->setFontFamily('Helvetica');
                $main->setBold();
                    
                $formatText =& $xlsBook->addFormat(array('Size' => 11));
                
                $cn = 0;
                $xls->write($rn, 0, "Laporan", $main);
                $xls->mergeCells($rn,0,$rn,10);
                $rn++;

                // Set up the headings of the columns
                foreach ( $titles as $t => $val){
                    $xls->setColumn($cn, $cn, $val);
                    $xls->write($rn, $cn++, $t, $format_bold);
                }
                $rn++;
                // reset the column num
                $cn = 0;
  $total = 0; 
            $i = 1;
            $js = 1;
                foreach ( $transactions as $trx ){
                    
                    $D=0;
                    $K=0;

                    if (strpos($trx["Mutation"]["note"],"Deposit")!==FALSE || strpos($trx["Mutation"]["note"],"Salah Transfer")!== FALSE 
                        || strpos($trx["Mutation"]["note"],"Salah Transfer")!==FALSE || strpos($trx["Mutation"]["note"],"Promo Cashback")!==FALSE 
                        || strpos($trx["Mutation"]["note"],"Kirim Saldo")!==FALSE || strpos($trx["Mutation"]["note"],"TOPUP")!==FALSE 
                        || strpos($trx["Mutation"]["note"],"Refund")!==FALSE || strpos($trx["Mutation"]["note"],"REKAPITULASI")!==FALSE) {
                        $ket = $trx["Mutation"]["note"];    
                    }else{
                        $ket = $trx["Product"]["nama_display"];
                    }

                    if ($trx['Product']['id'] == 100 || $trx['Product']['id'] == 355) {
                        $lembar = $trx[0]['lembar']; 
                        //$subtrx = $subtrx + $trx [0]['lembar'];
                    } else if ($trx['Product']['id'] == 101 || $trx['Product']['id'] == 102 || $trx['Product']['id'] == 104) {
                        $lembar = $trx[0]['lembar_telkom'];
                        //$subtrx = $subtrx + $trx [0]['count'];
                    } else {
                        $lembar =  $trx[0]['count'];
                        //$subtrx = $subtrx + $trx [0]['count'];
                    }

                    if (strpos($trx["Mutation"]["note"],"Deposit")!==FALSE || strpos($trx["Mutation"]["note"],"Salah Transfer")!==FALSE || strpos($trx["Mutation"]["note"],"Salah Transfer")!==FALSE || strpos($trx["Mutation"]["note"],"Promo Cashback")!==FALSE || strpos($trx["Mutation"]["note"],"Kirim Saldo")!==FALSE || strpos($trx["Mutation"]["note"],"TOPUP")!==FALSE || strpos($trx["Mutation"]["note"],"Refund")!==FALSE || strpos($trx["Mutation"]["note"],"REKAPITULASI")!==FALSE || strpos($trx["Mutation"]["note"],"TOPUP")!==FALSE) {
                        $nominal = $trx["Mutation"]["amount"]; 
                    } else if ($trx["Product"]["product_category_id"]==42||$trx["Product"]["product_category_id"]==62||$trx["Product"]["product_category_id"]==43) {
                        $nominal = $trx["Transaction"]["price_sell"];
                    } else {
                        $nominal = $trx["Product"]["nominal_display"];
                    }


                    if (!empty($trx["Inbox"]["idpel"])) {
                        $inbox = $trx["Inbox"]["idpel"];
                    } else {
                        $inbox = $trx["Inbox"]["msisdn"];
                    }

                    if ($trx["Mutation"]["amount"]<0) {
                        $D = $trx["Mutation"]["amount"];
                    }

                    if ($trx["Mutation"]["amount"]>=0) {
                        $K = $trx["Mutation"]["amount"];
                    }

                    if (strpos($trx["Mutation"]["note"],"Refund")!==FALSE) {
                        $saldo_akhir = $trx["Mutation"]["balance"];
                    } else {
                        $saldo_akhir = $trx["Mutation"]["balance"]+$trx["Mutation"]["amount"];
                    }

                    $saldo = $trx["Mutation"]["balance"];

                    $status = $trx["ResponseStat"]["name"];

                    $xls->write($rn, $cn++, $trx["Mutation"]["id"], $formatText);
                    $xls->write($rn, $cn++, $trx["Mutation"]["create_date"], $date_format);
                    $xls->write($rn, $cn++, $ket, $formatText);
                    $xls->write($rn, $cn++, $lembar, $formatText);
                    $xls->write($rn, $cn++, $nominal, $formatCurr);
                    $xls->write($rn, $cn++, $inbox, $formatText);
                    $xls->write($rn, $cn++, $D, $formatCurr);
                    $xls->write($rn, $cn++, $K, $formatCurr);
                    $xls->write($rn, $cn++, $saldo, $formatCurr);
                    $xls->write($rn, $cn++, $saldo_akhir, $formatCurr);
                    $xls->write($rn, $cn++, $status, $formatText);

					
					   if ($trx['Product']['id'] == 100 || $trx['Product']['id'] == 355 || $trx['Product']['id'] == 368 || $trx['Product']['id'] == 386) {
                    $jml_lembar += $trx[0]['lembar'];   
                } else if ($transactions['Product']['id'] == 101 || $transactions['Product']['id'] == 102 || $transactions['Product']['id'] == 104) {
                    $jml_lembar += $trx[0]['lembar_telkom'];
                } else {
                    $jml_lembar += $trx[0]['count'];
                }
				 $total += $trx["Transaction"]["price_sell"];
				
                    $i = $i+1;
            $js++;
			
			
                    // cycle to the next row
                    $rn++;
                    // Reset the column 
                    $cn = 0;
					
                }
                
                $grant = $rn+0;

                $main2 =& $xlsBook->addFormat(
                    array('Size' => 12,
                        'Align' => 'left',
                        'Color' => 'black',
                    ));
                $main2->setBold();
                //$xls->write($grant, 0, "Grant Total", $main);
                $xls->mergeCells($grant,0,$grant,10);
                $xls->write($grant, 0, "Grand Total", $main2);

                $startingExcelRow = 3;
                $finalExcelRow = $rn;

             

               

                $gTFormat =& $xlsBook->addFormat();
                $xls->write($rn+1,3,$jml_lembar,$formatCurr);
                $xls->write($rn+1,6,$total,$formatCurr);

			
                $xlsBook->close();          
                exit();
            }
            
        }
		
		
		  public function downline_reporting() {            
            $this->layout = 'ajax';

            /*if ($id=='reset') {
                $this->Session->delete('filter.date_start');
                $this->Session->delete('filter.date_end');
                $this->Session->delete('filter.username');
            }*/
            
            $tgl_start = date('Y-m-d 23');
            $tgl_end = date('Y-m-d');
            
            if (isset($_POST['date_start'])) {
                $tgl_start = $_POST['date_start'];
                $this->set('tgl_start',$tgl_start);
            }            
            if (isset($_POST['date_end'])) {
                $tgl_end = $_POST['date_end'];
                $this->set('tgl_end',$tgl_end);
            }
            if (isset($_POST['user_name']) and $_POST['user_name']!=='') {
                $user_name = $_POST['user_name'];
                $this->set('user_name',$user_name);
            }
            if (isset($_POST['user_id']) and $_POST['user_id']!=='') {
                $user_id = $_POST['user_id'];
                $this->set('user_id',$user_id);
            }

            $conditions = array();
            //$conditions['User.id'] = $this->Session->read('Auth.User.id');

            if (!isset($tgl_start)) {
                $conditions['Transaction.create_date >='] = date("m/d/Y")." 00:00:00";
            }
            else {
                $conditions['Transaction.create_date >='] = $tgl_start." 00:00:00";
            }
            
            if (!isset($tgl_end)) {
                $conditions['Transaction.create_date <='] = date("m/d/Y")." 23:59:59";
            }
            else {
                $conditions['Transaction.create_date <='] = $tgl_end." 23:59:59";
            }
            if (isset($user_name) and $user_name!=='') {
                $conditions['User.username ilike'] = $user_name;
                $conditions['User.upline'] = $this->Session->read('Auth.User.id');
            } else {
                $conditions['User.id'] = $this->Session->read('Auth.User.id');
            }
            if (date('Y-m-d', strtotime($tgl_start)) < date('Y-m-d')) {
                $this->Transaction->setDataSource('report'); 
            }
            $this->Transaction->recursive = -1;
            $dwn_reports = $this->Transaction->find('all', array(
                'conditions' => $conditions,
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'inner',
                        'conditions' => array('User.id = Transaction.user_id')
                    )
                ),
                'fields' => array(
                    'User.id', 
                    'User.username', 
                    'User.first_name',
                    'count("Transaction"."id") as jml_lembar',
                    'sum("Transaction"."price_sell") as jml_terpakai'
                ),
                'group' => array('User.id'),
                'order' => 'User.id asc'
            ));
            $this->set('dwn_reports',$dwn_reports);
        }

        public function downline_child() {
            $tgl_start = date('Y-m-d 23');
            $tgl_end = date('Y-m-d');
            
            if (isset($_POST['date_start'])) {
                $tgl_start = $_POST['date_start'];
                $this->set('tgl_start',$tgl_start);
            }            
            if (isset($_POST['date_end'])) {
                $tgl_end = $_POST['date_end'];
                $this->set('tgl_end',$tgl_end);
            }

            if (isset($_POST['user_id']) and $_POST['user_id']!=='') {
                $user_id = $_POST['user_id'];
                $this->set('user_id',$user_id);
            }

            $conditions_chd = array();
            $conditions_chd['User.upline'] = $user_id;

            if (!isset($tgl_start)) {
                $conditions_chd['Transaction.create_date >='] = date("m/d/Y")." 00:00:00";
            }
            else {
                $conditions_chd['Transaction.create_date >='] = $tgl_start." 00:00:00";
            }
            
            if (!isset($tgl_end)) {
                $conditions_chd['Transaction.create_date <='] = date("m/d/Y")." 23:59:59";
            }
            else {
                $conditions_chd['Transaction.create_date <='] = $tgl_end." 23:59:59"; 
            }
            if (date('Y-m-d', strtotime($tgl_start)) < date('Y-m-d')) {
                $this->Transaction->setDataSource('report'); 
            }
            $chd_reports = $this->Transaction->find('all', array(
                'conditions' => array_merge($conditions_chd),
                'joins' => array(
                        array(
                            'table' => 'users',
                            'alias' => 'User',
                            'type' => 'INNER',
                            'conditions' => array('User.id = Transaction.user_id')
                        ),
                        array(
                            'table' => 'mutations',
                            'alias' => 'Mutation',
                            'type' => 'FULL OUTER',
                            'conditions' => array('Mutation.user_id = User.id')
                        )
                ),
                'fields' => array(
                    'User.id', 'User.username', 'User.first_name',
                    //'concat("User"."first_name","-","User"."last_name") as full_name',
                    'sum("Mutation"."amount") as jml_saldo',
                    'count("Transaction"."id") as jlm_lembar',
                    'sum("Transaction"."price_sell") as jml_terpakai'
                ),
                'group' => array('Mutation.user_id','User.id')
            ));
            $this->set('chd_reports',$chd_reports);
        }

        public function downline_reporting_detail() {            
            $this->layout = 'ajax';
            
            $tgl_start = date('Y-m-d 23');
            $tgl_end = date('Y-m-d');
            
            if (isset($_POST['date_start'])) {
                $tgl_start = $_POST['date_start'];
                $this->set('tgl_start',$tgl_start);
            }            
            if (isset($_POST['date_end'])) {
                $tgl_end = $_POST['date_end'];
                $this->set('tgl_end',$tgl_end);
            }
            if (isset($_POST['user_id'])) {
                $user_id = $_POST['user_id'];
                $this->set('user_id',$user_id);
            }
            if (isset($_POST['user_name'])) {
                $user_name = $_POST['user_name'];
                $this->set('user_name',$user_name);
            }

            $conditions = array();

            if (!isset($tgl_start)) {
                $conditions['Transaction.create_date >='] = date("m/d/Y")." 00:00:00"; 
            }
            else {
                $conditions['Transaction.create_date >='] = $tgl_start." 00:00:00";  
            }
            
            if (!isset($tgl_end)) {
                $conditions['Transaction.create_date <='] = date("m/d/Y")." 23:59:59"; 
            }
            else {
                $conditions['Transaction.create_date <='] = $tgl_end." 23:59:59"; 
            }
            if (isset($user_id)) {
                $conditions['User.id'] = $user_id;
            }

            if (date('Y-m-d', strtotime($tgl_start)) < date('Y-m-d')) {
                $this->Transaction->setDataSource('report'); 
            }

            $this->Transaction->recursive = -1;

            //pln
            $report_plns = $this->Transaction->find('all', array(
                'conditions' => array_merge($conditions,array('Category.id=1','ProductCategory.id=42')),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT OUTER',
                        'conditions' => array('User.id = Transaction.user_id')
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array('Product.id = Transaction.product_id')
                    ),
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array('ProductCategory.id = Product.product_category_id')
                    ),
                    array(
                        'table' => 'categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array('Category.id = ProductCategory.category_id')
                    ),
                ),
                'fields' => array(
                    'User.username',
                    'Product.name',
                    'count("Transaction"."id") as jml_lembar',
                    'sum("Transaction"."price_sell") as jml_tagihan'
                ),
                'group' => array('User.username','Product.name'),
                'order' => array('Product.name ASC')
            ));                            
            $this->set('report_plns',$report_plns);

            //telkom
            $report_telkoms = $this->Transaction->find('all', array(
                'conditions' => array_merge($conditions,array('Category.id=4')),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT OUTER',
                        'conditions' => array('User.id = Transaction.user_id')
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array('Product.id = Transaction.product_id')
                    ),
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array('ProductCategory.id = Product.product_category_id')
                    ),
                    array(
                        'table' => 'categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array('Category.id = ProductCategory.category_id')
                    ),
                ),
                'fields' => array(
                    'User.username',
                    'Product.name',
                    'count("Transaction"."id") as jml_lembar',
                    'sum("Transaction"."price_sell") as jml_tagihan'
                ),
                'group' => array('User.username','Product.name'),
                'order' => array('Product.name ASC')
            ));                            
            $this->set('report_telkoms',$report_telkoms);

            //pulsa
            $report_pulsas = $this->Transaction->find('all', array(
                'conditions' => array_merge($conditions,array('Category.id=3')),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT OUTER',
                        'conditions' => array('User.id = Transaction.user_id')
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array('Product.id = Transaction.product_id')
                    ),
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array('ProductCategory.id = Product.product_category_id')
                    ),
                    array(
                        'table' => 'categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array('Category.id = ProductCategory.category_id')
                    ),
                ),
                'fields' => array(
                    'User.username',
                    'ProductCategory.name',
                    'count("Transaction"."id") as jml_lembar',
                    'sum("Transaction"."price_sell") as jml_tagihan'
                ),
                'group' => array('User.username','ProductCategory.name'),
                'order' => array('ProductCategory.name ASC')
            ));                            
            $this->set('report_pulsas',$report_pulsas);

            //game
            $report_games = $this->Transaction->find('all', array(
                'conditions' => array_merge($conditions,array('Category.id=2')),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT OUTER',
                        'conditions' => array('User.id = Transaction.user_id')
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array('Product.id = Transaction.product_id')
                    ),
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array('ProductCategory.id = Product.product_category_id')
                    ),
                    array(
                        'table' => 'categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array('Category.id = ProductCategory.category_id')
                    ),
                ),
                'fields' => array(
                    'User.username',
                    'ProductCategory.name',
                    'count("Transaction"."id") as jml_lembar',
                    'sum("Transaction"."price_sell") as jml_tagihan'
                ),
                'group' => array('User.username','ProductCategory.name'),
                'order' => array('ProductCategory.name ASC')
            ));                            
            $this->set('report_games',$report_games);

            //multifinance
            $mfinances = $this->Transaction->find('all', array(
                'conditions' => array_merge($conditions, array('Product.id IN (381, 384, 385 , 404, 405, 406,407,409,410,411,412,413)')),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT OUTER',
                        'conditions' => array('User.id = Transaction.user_id')
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array('Product.id = Transaction.product_id')
                    ),
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array('ProductCategory.id = Product.product_category_id')
                    ),
                    array(
                        'table' => 'categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array('Category.id = ProductCategory.category_id')
                    ),
                ),
                'fields' => array(
                    'User.username',
                    'ProductCategory.name',
                    'count("Transaction"."id") as jml_lembar',
                    'sum("Transaction"."price_sell") as jml_tagihan'
                ),
                'group' => array('User.username','ProductCategory.name'),
                'order' => array('ProductCategory.name ASC')
            ));                            
            $this->set('mfinances', $mfinances);

            //tvkabel
            $tvkabels = $this->Transaction->find('all', array(
                'conditions' => array_merge($conditions, array('Product.id=390')),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT OUTER',
                        'conditions' => array('User.id = Transaction.user_id')
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array('Product.id = Transaction.product_id')
                    ),
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array('ProductCategory.id = Product.product_category_id')
                    ),
                    array(
                        'table' => 'categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array('Category.id = ProductCategory.category_id')
                    ),
                ),
                'fields' => array(
                    'User.username',
                    'ProductCategory.name',
                    'count("Transaction"."id") as jml_lembar',
                    'sum("Transaction"."price_sell") as jml_tagihan'
                ),
                'group' => array('User.username','ProductCategory.name'),
                'order' => array('ProductCategory.name ASC')
            ));                            
            $this->set('tvkabels',$tvkabels);

            //pascabayar
            $ppcbayars = $this->Transaction->find('all', array(
                'conditions' => array_merge($conditions, array('Product.id IN (388, 389)')),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT OUTER',
                        'conditions' => array('User.id = Transaction.user_id')
                    ),
                    array(
                        'table' => 'products',
                        'alias' => 'Product',
                        'type' => 'LEFT',
                        'conditions' => array('Product.id = Transaction.product_id')
                    ),
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array('ProductCategory.id = Product.product_category_id')
                    ),
                    array(
                        'table' => 'categories',
                        'alias' => 'Category',
                        'type' => 'LEFT',
                        'conditions' => array('Category.id = ProductCategory.category_id')
                    ),
                ),
                'fields' => array(
                    'User.username',
                    'ProductCategory.name',
                    'count("Transaction"."id") as jml_lembar',
                    'sum("Transaction"."price_sell") as jml_tagihan'
                ),
                'group' => array('User.username','ProductCategory.name'),
                'order' => array('ProductCategory.name ASC')
            ));                            
            $this->set('ppcbayars',$ppcbayars);
        }
		
        public function struk($id) {
            $this->layout = "ajax";

            if (!$id) {
                $this->Session->setFlash(__('Invalid Transaction.', true));
                $this->redirect(array('action'=>'index'));
            }

            $trans = $this->Transaction->find('first', array(
                'conditions' => array('Transaction.id'=>$id),
                'fields'=>array('Transaction.inbox_id','Transaction.product_id','Transaction.sn','Transaction.id')
                )
            );

            $this->set('trans', $trans);

            // get product type
            $this->loadModel("Products");

            $prod = $this->Products->find('first',array(
                'conditions' => array('Products.id'=>$trans['Transaction']['product_id']),
                'fields' => array('ProductCategory.category_id', 'ProductCategory.code','Products.code'),
                'joins' => array(
                    array(
                        'table' => 'product_categories',
                        'alias' => 'ProductCategory',
                        'type' => 'LEFT',
                        'conditions' => array('Products.product_category_id = ProductCategory.id')
                    )
                )
            ));

            $this->set('product',$prod['ProductCategory']['category_id']);

            // set inbox_id
            $inbox_id = $trans['Transaction']['inbox_id'];

            // testing
            $code = $trans['Transaction']['product_id'];
            $this->set('code', $code);

            if ($prod['ProductCategory']['category_id']=="1") {
                // get the details
                if($trans['Transaction']['product_id'] == 80 || $trans['Transaction']['product_id'] == 354 
                    || $trans['Transaction']['product_id'] == 367 || $trans['Transaction']['product_id'] == 385 
                    || $trans['Transaction']['product_id'] == 357)
                {
                    $this->loadModel('PlnPrepaids');
                    $details = $this->PlnPrepaids->find('first',array(
                        'conditions'=>array('PlnPrepaids.inbox_id'=>$inbox_id)
                        )
                    );

                    $this->PlnPrepaids->query("update pln_prepaids set reprint = reprint+1 where inbox_id = ".$inbox_id);
                } else if ($trans['Transaction']['product_id'] == 100 || $trans['Transaction']['product_id'] == 355 
                    || $trans['Transaction']['product_id'] == 368 || $trans['Transaction']['product_id'] == 386)
                {
                
                    /*$this->loadModel('PlnPostpaids');
                    $details = $this->PlnPostpaids->find('first',array(
                        'conditions'=>array('PlnPostpaids.inbox_id'=>$inbox_id)
                    ));
                    $this->PlnPostpaids->query("update pln_postpaids set reprint = reprint+1 where inbox_id = ".$inbox_id);*/
                    
                    $this->loadModel("Outboxes");
                    $details = $this->Outboxes->find('first',array(
                        'conditions' => array('Outboxes.inbox_id'=>$inbox_id)
                        )
                    );

                    $xml = new SimpleXMLElement($details['Outboxes']['message']);
                    $this->set('xml', $xml);
                } else if ($trans['Transaction']['product_id'] == 105 || $trans['Transaction']['product_id'] == 356 
                    || $trans['Transaction']['product_id'] == 369 || $trans['Transaction']['product_id'] == 387)
                {
                    $this->loadModel('PlnNontaglists');
                    $details = $this->PlnNontaglists->find('first',array(
                        'conditions'=>array('PlnNontaglists.inbox_id'=>$inbox_id)
                        )
                    );

                    $this->PlnNontaglists->query("update pln_nontaglists set reprint = reprint+1 where inbox_id = ".$inbox_id);
                } else if ($trans['Transaction']['product_id'] == 252 || $trans['Transaction']['product_id'] == 253  
                    || $trans['Transaction']['product_id'] == 254 || $trans['Transaction']['product_id'] == 255 
                    || $trans['Transaction']['product_id'] == 256  || $trans['Transaction']['product_id'] == 257 
                    || $trans['Transaction']['product_id'] == 258 || $trans['Transaction']['product_id'] == 259 
                    || $trans['Transaction']['product_id'] == 260 || $trans['Transaction']['product_id'] == 261 
                    || $trans['Transaction']['product_id'] == 262 || $trans['Transaction']['product_id'] == 263 
                    || $trans['Transaction']['product_id'] == 264 || $trans['Transaction']['product_id'] == 265 
                    || $trans['Transaction']['product_id'] == 266 || $trans['Transaction']['product_id'] == 3540 
                    || $trans['Transaction']['product_id'] == 347 || $trans['Transaction']['product_id'] == 438
                    || $trans['Transaction']['product_id'] == 434
                ) {
                    // echo "ini struk pdam coy";
                    $this->loadModel('Pdams');
                    $details = $this->Pdams->find('first',array(
                        'conditions'=>array('Pdams.inbox_id'=>$inbox_id)
                    ));
                } else if ($trans['Transaction']['product_id'] == 375) {
                    $this->loadModel('Bpjs_kesehatans');
                    $details = $this->Bpjs_kesehatans->find('first',array(
                        'conditions'=>array('Bpjs_kesehatans.inbox_id'=>$inbox_id)
                        )
                    );
                } else if ($trans['Transaction']['product_id'] == 381 
                    || $trans['Transaction']['product_id'] == 384 || $trans['Transaction']['product_id'] == 385 ||$trans['Transaction']['product_id'] == 404 || $trans['Transaction']['product_id'] == 405 || $trans['Transaction']['product_id'] == 409 || $trans['Transaction']['product_id'] == 412 || $trans['Transaction']['product_id'] == 413 || $trans['Transaction']['product_id'] == 406 || $trans['Transaction']['product_id'] == 407 || $trans['Transaction']['product_id'] == 410 || $trans['Transaction']['product_id'] == 411) 
                {
                    $this->Transaction->recursive = -1;

                    $details = $this->Transaction->find('first', array(
                        'conditions'=>array('Transaction.id' => $id),
                        'joins' => array(
                            array(
                                'table' => 'outboxes',
                                'alias' => 'Outbox',
                                'type' => 'LEFT',
                                'conditions' => array('Outbox.inbox_id = Transaction.inbox_id')
                            ),
                            array(
                                'table' => 'products',
                                'alias' => 'Product',
                                'type' => 'LEFT',
                                'conditions' => array('Product.id = Transaction.product_id')
                            )
                        ),
                        'fields'=>array('Transaction.create_date','Outbox.message','Product.description','Outbox.id'),
                        'order'=>array('Outbox.id DESC')
                        )
                    );

                    $this->set('details', $details);
                    $mf = new SimpleXMLElement($details['Outbox']['message']);
                    $this->set('mf', $mf);
                    print($mf);
                }
                
                // tidak ada format loket
                $this->set('loket', $this->Session->read("Auth.User.company"));
                $this->set('details', $details);
            }
            else if ($prod['ProductCategory']['category_id']=="2") 
            {
                $this->Transaction->recursive = -1;
                $details = $this->Transaction->find('first',array(
                    'conditions'=>array('Transaction.id'=>$id),
                    'joins' => array(
                        array(
                            'table' => 'outboxes',
                            'alias' => 'Outbox',
                            'type' => 'LEFT',
                            'conditions' => array('Outbox.inbox_id = Transaction.inbox_id')
                            ),
                        array(
                            'table' => 'products',
                            'alias' => 'Product',
                            'type' => 'LEFT',
                            'conditions' => array('Product.id = Transaction.product_id')
                        )
                    ),
                    'fields'=>array('Transaction.create_date','Outbox.message','Product.description')
                    )
                );

                $this->set('details', $details);
                $games = new SimpleXMLElement($details['Outbox']['message']);

                // print_r($games);
                $data = array(
                    'voucher_code' => $games->voucher_code,
                    'serial_number' => $games->serial_number,
                    'voucher_password' => $games->voucher_password,
                    'pin' => $games->pin,
                    'stan' => $games->stan,
                    'hp'=> $games->hp
                );
                
                // print_r($data);
                $this->set('voc', $data);
            } else if ($prod['ProductCategory']['category_id']=="3") {
                $this->Transaction->recursive = -1;

                $details = $this->Transaction->find('first',array(
                    'conditions'=>array('Transaction.id'=>$id),
                    'joins' => array(
                        array(
                            'table' => 'outboxes',
                            'alias' => 'Outbox',
                            'type' => 'LEFT',
                            'conditions' => array('Outbox.inbox_id = Transaction.inbox_id')
                            ),
                        array(
                            'table' => 'products',
                            'alias' => 'Product',
                            'type' => 'LEFT',
                            'conditions' => array('Product.id = Transaction.product_id')
                            )
                        ),
                    'fields'=>array('Transaction.create_date','Outbox.message','Product.description','Outbox.id'),
                    'order'=>array('Outbox.id DESC')
                    )
                );

                $this->set('details',$details);

                $pulsa = new SimpleXMLElement($details['Outbox']['message']);
                $data = array(                    
                    'sn' => $pulsa->sn,                    
                    'stan' => $pulsa->stan,
                    'harga' => $pulsa->price,
                    'desc' => $pulsa->desc,
                    'msisdn' => $pulsa->msisdn,
                    'produk' => $pulsa->produk
                );

                $this->set('pulsa', $data);
            } else if ($prod['ProductCategory']['category_id']=="4") {
                $this->Transaction->recursive = -1;
                $details = $this->Transaction->find('first', array(
                    'conditions'=>array('Transaction.id' => $id),
                    'joins' => array(
                        array(
                            'table' => 'outboxes',
                            'alias' => 'Outbox',
                            'type' => 'LEFT',
                            'conditions' => array('Outbox.inbox_id = Transaction.inbox_id')
                        ),
                        array(
                            'table' => 'products',
                            'alias' => 'Product',
                            'type' => 'LEFT',
                            'conditions' => array('Product.id = Transaction.product_id')
                        )
                    ),
                    'fields'=>array('Transaction.create_date','Outbox.message','Product.description','Outbox.id'),
                    'order'=>array('Outbox.id DESC')
                    )
                );

                $this->set('details', $details);

                
                $tel= new SimpleXMLElement($details['Outbox']['message']);
                /* $data = array(                    
                    'idpel' => $telkom->idpelanggan,                    
                    'nama' => $telkom->namapelanggan,
                    'tes' => (string) $telkom->bulan_thn,
                    'jumlahtagihan' => $telkom->jumlahtagihan,
                    'jumlahadm' => $telkom->jumlahadm,
                    'jumlahbayar' => $telkom->jumlahbayar,
                    'noref' => $telkom->noreference,
                    'produk' => $telkom->produk
                ); */
                $this->set('tel', $tel);
            } else if ($trans['Transaction']['product_id'] == 390 || $trans['Transaction']['product_id'] == 418) {
                $this->Transaction->recursive = -1;

                $details = $this->Transaction->find('first', array(
                    'conditions'=>array('Transaction.id' => $id),
                    'joins' => array(
                        array(
                            'table' => 'outboxes',
                            'alias' => 'Outbox',
                            'type' => 'LEFT',
                            'conditions' => array('Outbox.inbox_id = Transaction.inbox_id')
                        ),
                        array(
                            'table' => 'products',
                            'alias' => 'Product',
                            'type' => 'LEFT',
                            'conditions' => array('Product.id = Transaction.product_id')
                        )
                    ),
                    'fields'=>array('Transaction.create_date','Outbox.message','Product.description','Outbox.id'),
                    'order'=>array('Outbox.id DESC')
                    )
                );

                $this->set('details', $details);

                $tvk = new SimpleXMLElement($details['Outbox']['message']);
                /*$data = array(                    
                    'sn' => $tvk->sn,                    
                    'stan' => $tvk->stan,
                    'harga' => $tvk->price,
                    'desc' => $tvk->desc,
                    'msisdn' => $tvk->msisdn,
                    'produk' => $tvk->produk
                );*/

                $this->set('tvk', $tvk);
            } else {
                $details = $trans['Transaction']['product_id'];
                var_dump($details);
            }

            $this->set('loket', $this->Session->read("Auth.User.company"));
            $this->set('details', $details);
        }
        
        public function struk_kai($id) {
            $this->layout = "ajax";

            if (!$id) {
                $this->Session->setFlash(__('Invalid Transaction.', true));
                $this->redirect(array('action'=>'index'));
            }

            $this->layout = 'ajax';
            $this->loadModel('Inbox');
            $this->loadModel('KaiTiket');
             
            $inboxnya = $this->Inbox->find('first',array(
                    'conditions' => array('Inbox.msisdn' => $id),
                    'fields' => array('Inbox.message','Inbox.msisdn','Inbox.create_date')
            ));
        
            $details = $this->KaiTiket->find('all',array('conditions' => array('KaiTiket.kode_booking' => $id)));


            $this->set('loket', $this->Session->read("Auth.User.company"));
            $this->set('details', $details);
            $this->set('inbox', $inboxnya);
        }

        public function list_reprint() {
            $this->layout = 'ajax';

            $tgl_start = date('Y-m-d 00:00:00');
            $tgl_end = date('Y-m-d 23:59:59');
            $id_pel = "0";
            $produk_id = "0";
            
            if (isset($_POST['bulan'])) {
                $bulan = $_POST['bulan'];
            }
            
            if (isset($_POST['tahun'])) {
                $tahun = $_POST['tahun'];
            }
            
            if (isset($_POST['date_start'])) {
                $tgl_start = $_POST['date_start'];
            }
            
            if (isset($_POST['date_end'])) {
                $tgl_end = $_POST['date_end'];
            }
            
            if (isset($_POST['idpel'])) {
                $id_pel = $_POST['idpel'];
            }
            
            if (isset($_POST['produk_id'])) {
                $produk_id = $_POST['produk_id'];
            }
            
            $conditions = array();
            $conditions['Transaction.user_id'] = $this->Session->read('Auth.User.id');
            // $conditions['Transaction.product_id'] = $produk_id;
            $conditions['Inbox.prod_id'] = $produk_id;
            $conditions['Inbox.message LIKE '] = "%".$id_pel."%";
                        
            /*if (!isset($tgl_start)) {
               $conditions['Transaction.create_date >='] = date("m/d/Y")." 00:00:00";                 
            }
            else {
               $conditions['Transaction.create_date >='] = $tgl_start." 00:00:00";                 
            }

            if (!isset($tgl_end)) {
               $conditions['Transaction.create_date <='] = date("m/d/Y")." 23:59:59";                
            }
            else {
               $conditions['Transaction.create_date <='] = $tgl_end." 23:59:59";               
            }*/
            
            /* if (isset($bulan) && $tahun) {
                $conditions['extract(month from "Transaction"."create_date") ='] = $bulan;
                $conditions['extract(year from "Transaction"."create_date") ='] = $tahun;
            } */

            $date = date("m/d/Y");
            
            // $conditions['Transaction.create_date >='] = date("m/d/Y")." 00:00:00";                
			$conditions['Transaction.create_date >='] = date("m/d/Y", strtotime($date."-3 days"))." 00:00:00";                
            $conditions['Transaction.create_date <='] = date("m/d/Y")." 23:59:59";        
			if (date('Y-m-d', strtotime($tgl_start)) < date('Y-m-d')) {
                $this->Transaction->setDataSource('report'); 
            }
            $this->Transaction->recursive = 0;
            
            $transactions = $this->Transaction->find('all',array(
                'conditions'=> $conditions,
                'fields' => array('Transaction.inbox_id','Transaction.id','Transaction.product_id','Inbox.prod_id','Inbox.message','Product.name','Transaction.create_date','Transaction.remark','Transaction.price_sell','Inbox.idpel','Inbox.msisdn'),
                'order' => array('Transaction.create_date asc'),
                #'limit' => 10
            ));
            
            $this->set('transactions', $transactions);
        }
        
        public function reprint() {
		
		  $this->loadModel("Product_categories");
                $product_categories_game = $this->Product_categories->find('all',array(
				'conditions'=>array('Product_categories.category_id'=>2),
                    'fields'=> array('Product_categories.id','Product_categories.name','Product_categories.category_id'),
                    'order' => array('Product_categories.name')
					
                    ));
                $this->set(compact('product_categories_game'));
                
				$product_categories_pul = $this->Product_categories->find('all',array(
				'conditions'=>array('Product_categories.category_id'=>3),
                    'fields'=> array('Product_categories.id','Product_categories.name','Product_categories.category_id'),
                    'order' => array('Product_categories.name')
					
                    ));
                $this->set(compact('product_categories_pul'));
                
				
				
            $this->layout = "ajax";
            $this->loadModel("Products");
            $products = $this->Products->find('all',array(
                'fields'=> array('Products.id','Products.code','Products.description'),
                'order' => array('Products.code')
                ));
            $this->set(compact('products'));
        }

         public function data_kai() {
            $this->layout = 'ajax';
            $this->loadModel('Inbox');
            $this->loadModel('KaiTiket');

            //$tgl_start = date('Y-m-d 00:00:00');
            //$tgl_end = date('Y-m-d 23:59:59');
            $kode_booking = "";
            $noktp = "";
            
            if (isset($_POST['kodebooking'])) {
                $kode_booking = $_POST['kodebooking'];
            }
            
            if (isset($_POST['noktp'])) {
                $noktp = $_POST['noktp'];
            }
            

            if (isset($_POST['noktp']) && $_POST['kodebooking']==null) {
                
                $conditions_inbox = array();
                $conditions_inbox['Inbox.message LIKE '] = "%".$noktp."%";
                $conditions_inbox['Inbox.prod_id'] = 399;

                $this->Inbox->recursive = -1;
                $inboxnya = $this->Inbox->find('first',array(
                    'conditions' => $conditions_inbox,
                    'fields' => array('Inbox.message','Inbox.msisdn')
                ));

                $datas = $this->KaiTiket->find('all',array('conditions' => array('KaiTiket.kode_booking' => $inboxnya['Inbox']['msisdn'])));
                $this->set('datas',$datas);
            
            } else if (isset($_POST['kodebooking'])  && $_POST['noktp']==null) {
                
                $datas = $this->KaiTiket->find('all',array('conditions' => array('KaiTiket.kode_booking' => $kode_booking)));
                $this->set('datas',$datas);
            
            } else {
                
                $conditions_inbox = array();
                $conditions_inbox['Inbox.message LIKE '] = "%".$noktp."%";
                $conditions_inbox['Inbox.prod_id'] = 399;

                $this->Inbox->recursive = -1;
                $cek_noktp = $this->Inbox->find('first',array(
                    'conditions' => $conditions_inbox,
                    'fields' => array('Inbox.message','Inbox.msisdn')
                ));
          
                $cek_kodebooking = $this->KaiTiket->find('first',array('conditions' => array('KaiTiket.kode_booking' => $kode_booking), 'fields' => array('KaiTiket.kode_booking')));

                if($cek_noktp['Inbox']['msisdn']==$cek_kodebooking['KaiTiket']['kode_booking']){
                   

                    $datas = $this->KaiTiket->find('all',array('conditions' => array('KaiTiket.kode_booking' => $cek_noktp['Inbox']['msisdn'])));
                    $this->set('datas',$datas);
                }
                
            }   
            
        }

        public function reprint_KAI(){
            $this->layout = 'ajax';
        }
        
              public function trx() {
            
            //initialize
            $trx_id = "0";
            $nominal = "";
            $hp = "";
            $account_no = "";
            $kolektif = "";
            $bit48 = "";
            $bit62 = "";
			$bit11 = "";
            $bit12 = "";
            $unsold = "";
			 $cekidpel = "";
			 $inputtoken="";
			  $extend = "";
			 $nik = "";
			  $cmd = "";
            
			 $lokasi_kerja = "";
			 
			 $kode_bayar = "";
             $biaya_trx = "";
             $jum_bayar = "";
            $total_bayar = "";
			 $kantor_cabang_alamat = "";
			 $jaminan = "";
			 
			 $periode_payment = "";

            $stasiunawal = "";
            $stasiunakhir = "";
            $tgl_order = "";
            $tiketdewasa = "";
            $tiketinfant = "";

            $sn_cek = "";
            $no_ka = "";
            $subkelas_ka = "";
            $namadewasa1 = "";
            $noktp1 = "";
            $dewasa_tgl1 = "";
            $nohenpon1 = "";
            $namadewasa2 = "";
            $noktp2 = "";
            $dewasa_tgl2 = "";
            $nohenpon2 = "";
            $namadewasa3 = "";
            $noktp3 = "";
            $dewasa_tgl3 = "";
            $nohenpon3 = "";
            $namadewasa4 = "";
            $noktp4 = "";
            $dewasa_tgl4 = "";
            $nohenpon4 = "";
            $namainfant1 = "";
            $infant_tgl1 = "";
            $namainfant2 = "";
            $infant_tgl2 = "";
            $sn_inq = "";
            $kode_booking = "";
			 
            $this->layout = 'ajax';
            if($_POST['product_id']==101 || $_POST['product_id']==102 || $_POST['product_id']==104){
                $apiUrl   = Configure::read("mains.server")."/Transactions/trx.xml";
            }else{
                $apiUrl   = Configure::read("mains.server")."/Transactions/trx.xml";
            }
            
            $username = $this->Session->read("Auth.User.username");
            $password = $this->Session->read("userPass");
            $key      = $this->Session->read("Auth.User.secret_key");
            // $username = "pids13100175";
            // $password = "aldwin.3";
            // $key      = "abcd1234";
           
            $inputtoken = $_POST['inputtoken'];
            $periode_payment = $_POST['periode_payment'];
            $kode_bayar = $_POST['kode_bayar'];
            $biaya_trx = $_POST['biaya_trx'];
            $jum_bayar = $_POST['jum_bayar'];
            $total_bayar = $_POST['total_bayar'];
            $kantor_cabang_alamat = $_POST['kantor_cabang_alamat'];
            $jaminan = $_POST['jaminan'];
            $nik = $_POST['nik'];
            $extend = $_POST['extend'];
            $lokasi_kerja = $_POST['lokasi_kerja'];
			$cmd = $_POST['cmd'];
            $product_id = $_POST['product_id'];
            $category_id = $_POST['category_id'];
            $trx_date   = date("YmdHis");
            $trx_type   = $_POST['trx_type'];
            $cekidpel   = $_POST['cekidpel'];

            if (isset($_POST['trx_id'])) {
                $trx_id     = $_POST['trx_id'];
            }
            if (isset($_POST['nominal'])) {
                $nominal    = $_POST['nominal'];
            }
            if (isset($_POST['hp'])) {
                $hp         = $_POST['hp'];
            }
            if (isset($_POST['category_id'])) {
                $category_id = $_POST['category_id'];
            }
            if (isset($_POST['account_no'])) {
                $account_no = $_POST['account_no'];
            }
            if (isset($_POST['kolektif'])) {
                $kolektif = $_POST['kolektif'];
            }
            if (isset($_POST['unsold'])) {
                $unsold    = $_POST['unsold'];
            }
            if (isset($_POST['bit48'])) {
                $bit48    = $_POST['bit48'];
            }
            if (isset($_POST['bit62'])) {
                $bit62    = $_POST['bit62'];
            }
			if (isset($_POST['bit11'])) {
                $bit11    = $_POST['bit11'];
            }
            if (isset($_POST['bit12'])) {
                $bit12    = $_POST['bit12'];
            }
			
			if (isset($_POST['periode_payment'])) {
                $periode_payment    = $_POST['periode_payment'];
            }
			
			
			if (isset($_POST['inputtoken'])) {
                $inputtoken    = $_POST['inputtoken'];
            }
            if (isset($_POST['stasiunawal'])){
                $stasiunawal = $_POST['stasiunawal'];            
            }
            if (isset($_POST['stasiunakhir'])){
                $stasiunakhir = $_POST['stasiunakhir'];
            }
            if (isset($_POST['tgl_order'])){
                $tgl_order = $_POST['tgl_order'];
            }
            if (isset($_POST['tiketdewasa'])){
                $tiketdewasa = $_POST['tiketdewasa'];
            }
            if (isset($_POST['tiketinfant'])){
                $tiketinfant = $_POST['tiketinfant'];
            }
            if (isset($_POST['sn_cek'])){
                $sn_cek = $_POST['sn_cek'];
            }
            if (isset($_POST['no_ka'])){
                $no_ka = $_POST['no_ka'];
            }
            if (isset($_POST['subkelas_ka'])){
                $subkelas_ka = $_POST['subkelas_ka'];
            }
            if (isset($_POST['namadewasa1'])){
                $namadewasa1 = $_POST['namadewasa1'];
            }
            if (isset($_POST['noktp1'])){
                $noktp1 = $_POST['noktp1'];
            }
            if (isset($_POST['dewasa_tgl1'])){
                $dewasa_tgl1 = $_POST['dewasa_tgl1'];
            }
            if (isset($_POST['nohenpon1'])){
                $nohenpon1 = $_POST['nohenpon1'];
            }
            if (isset($_POST['namadewasa2'])){
                $namadewasa2 = $_POST['namadewasa2'];
            }
            if (isset($_POST['noktp2'])){
                $noktp2 = $_POST['noktp2'];
            }
            if (isset($_POST['dewasa_tgl2'])){
                $dewasa_tgl2 = $_POST['dewasa_tgl2'];
            }
            if (isset($_POST['nohenpon2'])){
                $nohenpon2 = $_POST['nohenpon2'];
            }
            if (isset($_POST['namadewasa3'])){
                $namadewasa3 = $_POST['namadewasa3'];
            }
            if (isset($_POST['noktp3'])){
                $noktp3 = $_POST['noktp3'];
            }
            if (isset($_POST['dewasa_tgl3'])){
                $dewasa_tgl3 = $_POST['dewasa_tgl3'];
            }
            if (isset($_POST['nohenpon3'])){
                $nohenpon3 = $_POST['nohenpon3'];
            }
            if (isset($_POST['namadewasa4'])){
                $namadewasa4 = $_POST['namadewasa4'];
            }
            if (isset($_POST['noktp4'])){
                $noktp4 = $_POST['noktp4'];
            }
            if (isset($_POST['dewasa_tgl4'])){
                $dewasa_tgl4 = $_POST['dewasa_tgl4'];
            }
            if (isset($_POST['nohenpon4'])){
                $nohenpon4 = $_POST['nohenpon4'];
            }
            if (isset($_POST['namainfant1'])){
                $namainfant1 = $_POST['namainfant1'];
            }
            if (isset($_POST['infant_tgl1'])){
                $infant_tgl1 = $_POST['infant_tgl1'];
            }
            if (isset($_POST['namainfant2'])){
                $namainfant2 = $_POST['namainfant2'];
            }
            if (isset($_POST['infant_tgl2'])){
                $infant_tgl2 = $_POST['infant_tgl2'];
            }
            if (isset($_POST['sn_inq'])){
                $sn_inq = $_POST['sn_inq'];
            }
            if (isset($_POST['kode_booking'])){
                $kode_booking = $_POST['kode_booking'];
            }
            if (isset($_POST['nominals'])){
                $nominals = $_POST['nominals'];
            }


            $signature = md5($username.$password.$product_id.$trx_date.$key);

            $post_data = array(
                'trx_date' => $trx_date,                
                'trx_type' => $trx_type,
                'trx_id' => $trx_id,
                'cust_msisdn' => $hp,
                'cust_account_no' => $account_no,
                'product_id' => $product_id,
                'unsold' => $unsold,
                'media' => 'WEBTRX',
				'cmd' => $cmd,
                'bit11' => $bit11,
                'bit12' => $bit12,
                'bit48' => $bit48,
                'bit62' => $bit62,
                'product_nomination' => $nominal,
                'stasiunawal' => $stasiunawal,
                'stasiunakhir' => $stasiunakhir,
                'tgl_order' => $tgl_order,
                'tiketdewasa' => $tiketdewasa,
                'tiketinfant' => $tiketinfant,
				'periode_payment' => $periode_payment,
                'sn_cek' => $sn_cek,
                'no_ka' => $no_ka,
                'subkelas_ka' => $subkelas_ka,
                'namadewasa1' => $namadewasa1,
                'noktp1' => $noktp1,
                'dewasa_tgl1' => $dewasa_tgl1,
                'nohenpon1' => $nohenpon1,
                'namadewasa2' => $namadewasa2,
                'noktp2' => $noktp2,
                'dewasa_tgl2' => $dewasa_tgl2,
                'nohenpon2' => $nohenpon2,
                'namadewasa3' => $namadewasa3,
                'noktp3' => $noktp3,
                'dewasa_tgl3' => $dewasa_tgl3,
                'nohenpon3' => $nohenpon3,
                'namadewasa4' => $namadewasa4,
                'noktp4' => $noktp4,
                'dewasa_tgl4' => $dewasa_tgl4,
                'nohenpon4' => $nohenpon4,
                'namainfant1' => $namainfant1,
                'infant_tgl1' => $infant_tgl1,
                'namainfant2' => $namainfant2,
                'infant_tgl2' => $infant_tgl2,
                'sn_inq' => $sn_inq,
                'kode_booking' => $kode_booking,
                'nominals' => $nominals
            );
      
            $post_data = http_build_query($post_data,'', '&');
            $curl = curl_init($apiUrl);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, $apiUrl);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
              'Authorization: '.'PELANGIREST username='.$username.'&password='.$password.'&signature='.$signature
            ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            
            // echo "<pre>";
            // print_r($response);
            // exit();
            // echo "</pre>";
			
            $this->set('periode_payment',$periode_payment);
			$this->set('lokasi_kerja',$lokasi_kerja);
            $this->set('nik',$nik);
            $this->set('cmd',$cmd);
            $this->set('cekidpel',$cekidpel);
            $this->set('response',$response);
            $this->set('product_id',$product_id);
            $this->set('trx_date',$trx_date);
            $this->set('trx_type',$trx_type);
            $this->set('trx_id',$trx_id);
            $this->set('nominal',$nominal);
            $this->set('hp',$hp);
            $this->set('account_no',$account_no);
            $this->set('kolektif',$kolektif);
			$this->set('category_id',$category_id);
			$this->set('kode_bayar',$kode_bayar);
            $this->set('jum_bayar',$jum_bayar);
			$this->set('biaya_trx',$biaya_trx);
			$this->set('total_bayar',$total_bayar);
			$this->set('kantor_cabang_alamat',$kantor_cabang_alamat);
			$this->set('jaminan',$jaminan);
            $this->set('account_no',$account_no);
            $this->set('extend',$extend);
            $this->set('inputtoken',$inputtoken);
            $this->set('product_id',$product_id);
            $this->set('nominals',$nominals);
		
		}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Transaction->recursive = 0;
		$this->set('transactions', $this->Paginator->paginate());
                
				
				
				$this->loadModel("Product_categories");
                $product_categories_game = $this->Product_categories->find('all',array(
				'conditions'=>array('Product_categories.category_id'=>2),
                    'fields'=> array('Product_categories.id','Product_categories.name','Product_categories.category_id'),
                    'order' => array('Product_categories.name')
					
                    ));
                $this->set(compact('product_categories_game'));
                
				$product_categories_pul = $this->Product_categories->find('all',array(
				'conditions'=>array('Product_categories.category_id'=>3),
                    'fields'=> array('Product_categories.id','Product_categories.name','Product_categories.category_id'),
                    'order' => array('Product_categories.name')
					
                    ));
                $this->set(compact('product_categories_pul'));
                
                ////if(strpos($this->Session->read("Auth.User.username"),"sca02")!==FALSE || strpos($this->Session->read("Auth.User.username"),"sca01")!==FALSE || strpos($this->Session->read("Auth.User.username"),"pid029")!==FALSE){
					//if(strpos($this->Session->read("Auth.User.username"),"sca02")!==FALSE || strpos($this->Session->read("Auth.User.username"),"sca01")!==FALSE){
					if(strpos($this->Session->read("Auth.User.username"),"sca04")!==FALSE){
				////if(substr($this->Session->read("Auth.User.username"),5)=="sca02" || substr($this->Session->read("Auth.User.username"),5)=="sca01" || substr($this->Session->read("Auth.User.username"),6)=="pid029"){
                $this->loadModel("Products");
                $products = $this->Products->find('all',array(
                    'fields'=> array('Products.id','Products.code','Products.product_category_id','Products.description'),
                    'order' => array('Products.code'),
                    'conditions' => array('Products.id'=> array('367','368','369'))
					));
                $this->set(compact('products'));
			}else if(strpos($this->Session->read("Auth.User.username"),"sca")!==FALSE){
				////if(substr($this->Session->read("Auth.User.username"),5)=="sca02" || substr($this->Session->read("Auth.User.username"),5)=="sca01" || substr($this->Session->read("Auth.User.username"),6)=="pid029"){
                $this->loadModel("Products");
                $products = $this->Products->find('all',array(
                    'fields'=> array('Products.id','Products.code','Products.product_category_id','Products.description'),
                    'order' => array('Products.code'),
                    'conditions' => array('Products.id'=> array('354','355','356'))
					));
                $this->set(compact('products'));
			}else {
			 $this->loadModel("Products");
                $products = $this->Products->find('all',array(
                    'fields'=> array('Products.id','Products.code','Products.product_category_id','Products.description'),
                    'order' => array('Products.code')
					));
                $this->set(compact('products'));
			}
				
				

            //$this->set(compact('products'));
            $this->set('productbycategory',$productbycategory);

                $this->set('main_server',Configure::read('main.server'));
                
                $this->loadModel("Kolektifs");
                $kolektif = $this->Kolektifs->find('all',array(
                    'conditions' => array('Kolektifs.user_id'=> $this->Session->read("Auth.User.id"))
                ));
                $this->set('kolektif_list',$kolektif);
	
				$this->set('nama_depan',$this->Session->read("Auth.User.first_name"));
				$this->set('nama_blkng',$this->Session->read("Auth.User.last_name"));
                                
                $empty_position = $this->getRandomLocation();
      
                $this->set('emptiness',$empty_position);
               
	
	}
	
 function productByCategory($id = null) {
           // $this->layout = 'ajax';
 	    $this->Transaction->recursive = 0;
$this->loadModel("Products");
 
 $productbycategory = $this->Products->find('all',array(
              'conditions'=>array('Products.product_category_id'=>$id,'Products.name != '=>"",'Products.code != '=>'TOKENPLNX','Products.active'=>true),
                'fields' => array('Products.id','Products.code','Products.description','Products.active'),
                'order' => array('Products.id')
            ));

            //$this->set(compact('products'));
            $this->set('productbycategory',$productbycategory);

        }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Transaction->exists($id)) {
			throw new NotFoundException(__('Invalid transaction'));
		}
		$options = array('conditions' => array('Transaction.' . $this->Transaction->primaryKey => $id));
		$this->set('transaction', $this->Transaction->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Transaction->create();
			if ($this->Transaction->save($this->request->data)) {
				$this->Session->setFlash(__('The transaction has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.'));
			}
		}
		$users = $this->Transaction->User->find('list');
		$products = $this->Transaction->Product->find('list');
		$inboxes = $this->Transaction->Inbox->find('list');
		$this->set(compact('users', 'products', 'inboxes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Transaction->exists($id)) {
			throw new NotFoundException(__('Invalid transaction'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Transaction->save($this->request->data)) {
				$this->Session->setFlash(__('The transaction has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Transaction.' . $this->Transaction->primaryKey => $id));
			$this->request->data = $this->Transaction->find('first', $options);
		}
		$users = $this->Transaction->User->find('list');
		$products = $this->Transaction->Product->find('list');
		$inboxes = $this->Transaction->Inbox->find('list');
		$this->set(compact('users', 'products', 'inboxes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 
 	 public function manualadvice() {
	 
   $this->layout = "ajax";
							
							
	 }
 
  public function update_manual() {
  	$inboxid ="";
	$inboxid = $_POST['inboxid'];
	
	
    $this->loadModel('Inboxes');
    if (isset($_POST['inboxid'])) {
                $inboxid = $_POST['inboxid'];
            }            
  
     $this->Inboxes->query("update inboxes set status = 200 where id = ".$inboxid);
  }
 
 
  public function list_manualadvice() {

     $this->layout = "ajax";

     $idpel="";
     if (isset($_POST['idpel'])) {
        $idpel = $_POST['idpel'];
    }            
            $this->set('tgl_start',$tgl_start);
            $this->set('tgl_end',$tgl_end);
            $this->set('idpel',$idpel);
            $conditions = array();
            $conditions['Outboxes.user_id'] = $this->Session->read('Auth.User.id');
            $conditions['Inbox.status'] = 901;
            $conditions2 = array();
            $conditions['Outboxes.create_date >='] = date("m/d/y")." 00:00:00";
            $conditions['Inbox.idpel'] = $idpel;				       
            $conditions['Outboxes.create_date <='] = date("m/d/y")." 23:59:59"; 
            $conditions2 = array("or"=>array(
                "Outboxes.message LIKE"=>"%<code>0005</code>%",
                "Outboxes.message LIKE"=>"%<code>0068</code>%"));
            $conditions2 = array("( Outboxes.message like '%<code>0005</code>%' or Outboxes.message like '%<code>0068</code>%' )");
            $conditionsmain1 = array_merge($conditions,$conditions2);
			//$this->Transaction->recursive = -1;
            $this->loadModel("Outboxes");

            $outboxes = $this->Outboxes->find('all',array(
                'conditions'=>  $conditionsmain1,
                'joins' => array(
                    array(
                        'table' => 'inboxes',
                        'alias' => 'Inbox',
                        'type' => 'left',
                        'conditions' => array('Outboxes.inbox_id = Inbox.id')
                    )
                ),
                'fields' => array(
                 'Outboxes.message',
                 'Outboxes.create_date',
                 'Inbox.nominal',
                 'Outboxes.inbox_id',
                 'Inbox.idpel',
                 'Inbox.id'
             ),

                'order'=>array('Outboxes.message')
            ));
            $this->set('outboxes', $outboxes);
        }
        
        function kai_index(){

           $this->loadModel("Stasiuns");
           $stasiuns_asal = $this->Stasiuns->find('all',array(
            'conditions'=>array('Stasiuns.origin'=>true),
            'fields'=> array('Stasiuns.id','Stasiuns.nama','Stasiuns.code','Stasiuns.origin','Stasiuns.destination'),
                    //'order' => array('Stasiuns.nama')

        ));
           $this->set(compact('stasiuns_asal'));

           $stasiuns_tujuan = $this->Stasiuns->find('all',array(
            'conditions'=>array('Stasiuns.destination'=>true),
            'fields'=> array('Stasiuns.id','Stasiuns.nama','Stasiuns.code','Stasiuns.origin','Stasiuns.destination'),
                    //'order' => array('Stasiuns.nama')

        ));
           $this->set(compact('stasiuns_tujuan'));


           if($this->request->data['Kai']['pergi']){
            $conditions["Kai.pergi >="] = date("d-m-Y", strtotime($this->request->data['Kai']['pergi']))." 00:00:00";
            $this->Session->write('kai.pergi', $conditions["Kai.pergi >="]);
        }else{
            $conditions["Kai.pergi >="] = date("d-m-Y")." 00:00:00";
            $this->request->data['Kai']['pergi'] = date("Y/m/d", strtotime($this->request->data['Kai']['pergi']." 00:00:00"));
        }   
        //for write end date         
        if($this->request->data['Kai']['pulang']){
            $conditions["Kai.pulang <="] = date("d-m-Y", strtotime($this->request->data['Kai']['pulang']))." 23:59:59";
            $this->Session->write('Kai.pulang', $conditions["Kai.pulang <="]);
        }else{
            $conditions["Kai.pulang <="] = date("d-m-Y")." 23:59:59";
            $this->request->data['Kai']['pulang'] = date("Y/m/d", strtotime($this->request->data['Kai']['pulang']." 00:00:00"));
        }
    }
 
	public function delete($id = null) {
		$this->Transaction->id = $id;
		if (!$this->Transaction->exists()) {
			throw new NotFoundException(__('Invalid transaction'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Transaction->delete()) {
			$this->Session->setFlash(__('The transaction has been deleted.'));
		} else {
			$this->Session->setFlash(__('The transaction could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}

	
	
