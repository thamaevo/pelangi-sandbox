<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>

<div >
	<p>
<b>Berikut beberapa parameter yang harus anda kirim untuk API H2H :</b>

<br>
<br>
<br>
<b>Request</b>
<br>
</p>
<div class="col-md-11">
<div class="row">
<!-- <div class="widget-block"> -->
	<!-- <div class="widget-head"></div> -->
	<!-- <div class="widget-content"> -->
		<!-- <div class="widget-box"> -->
			<table class="table-bordered boderNon rounded table-responsive table" id="Request" style="width:100%">
				<thead class="thead-dark" align="center">
					<tr>
						<th rowspan="2">Field</th>
						<th rowspan="2">Data Field</th>
						<th colspan="7">Mandatory (M)</th>
						<th rowspan="2">Format</th>
						<th rowspan="2">Description</th>
					</tr>
					<tr>
						<th>PLN</th>
						<th>Telkom</th>
						<th>BPJS</th>
						<th>PDAM</th>
						<th>Pulsa</th>
						<th>Voucher Game</th>
						<th>Multifinance</th>
					</tr>
				</thead>
				<tbody class="bg-white" align="center">
					<tr>
						<td>trx_date</td>
						<td>String(14)</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>format = YYYYMMDDHHMMSS <br>contoh = 20201015170114</td>
						<td>Waktu Transaksi</td>
					</tr>
					<tr>
						<td>trx_type</td>
						<td>String(4)</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>format = XXXX contoh = 2100</td>
						<td>Kode Transaksi</td>
					</tr>
					<tr>
						<td>trx_id</td>
						<td>String(10)</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>format = XXXXXXXXXX <br> contoh = 7180246359</td>
						<td>Nomer Transaksi</td>
					</tr>
					<tr>
						<td>cust_msisdn</td>
						<td>String(12)</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>format = XXXXXXXXXXXX <br>contoh = 081612990096</td>
						<td>Nomor pelanggan, nomor handphone atau nomor billing</td>
					</tr>
					<tr>
						<td>cust_account_no</td>
						<td>String(12)</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>format = XXXXXXXXXXXX <br>contoh = 081612990096</td>
						<td>ID Pelanggan</td>
					</tr>
					<tr>
						<td>product_id</td>
						<td>String(3)</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>format = XXX <br>contoh = 229</td>
						<td>Kode Product</td>
					</tr>
					<tr>
						<td>product_nomination</td>
						<td>String(11)</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>M</td>
						<td>contoh = 20000</td>
						<td>Nominal Tagihan</td>
					</tr>
					<tr>
						<td>periode_payment</td>
						<td>String(2)</td>
						<td>-</td>
						<td>-</td>
						<td>M</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>-</td>
						<td>2 digit (01-12)</td>
						<td>Periode bayar</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
<!-- </div>
</div>
</div> -->

<br>
<br>
<br>
<p>
	<b>Response</b>
</p>

<div class="widget-block">
	<div class="widget-head"></div>
	<div class="widget-content">
		<div class="widget-box">
			<table class="table table-bordered boderNon rounded" id="Response">
				<thead class="thead-dark">
					<tr>
						<th>Field</th>
						<th>Data Field</th>
						<th>Mandatory</th>
						<th>Description</th>
					</tr>
				</thead>
				<tbody class="bg-white">
					<tr>
						<td>trx_id</td>
						<td>String(10)</td>
						<td>Y</td>
						<td>ID Transaksi</td>
					</tr>
					<tr>
						<td>stan</td>
						<td>String(12)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>datetime</td>
						<td>String(14)</td>
						<td>Y</td>
						<td>Waktu Transaksi</td>
					</tr>
					<tr>
						<td>merchant_code</td>
						<td>String(4)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>bank_code</td>
						<td>String(7)</td>
						<td>Y</td>
						<td>Kode Bank</td>
					</tr>
					<tr>
						<td>rc</td>
						<td>String(4)</td>
						<td>Y</td>
						<td>Kode Status Transaksi. Format : Number, 4</td>
					</tr>
					<tr>
						<td>terminal_id</td>
						<td>String(16)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>material_number</td>
						<td>String(11)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>subscriber_id</td>
						<td>String(12)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>pln_refno</td>
						<td>String(32)</td>
						<td>Y</td>
						<td><p class="text-break"></p></td>
					</tr>
					<tr>
						<td>switcher_refno</td>
						<td>String(32)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>subscriber_name</td>
						<td>String(25)</td>
						<td>Y</td>
						<td>Nama Pelanggan</td>
					</tr>
					<tr>
						<td>subscriber_segmentation</td>
						<td>String(4)</td>
						<td>Y</td>
						<td>Sekmen Pelanggan</td>
					</tr>
					<tr>
						<td>power</td>
						<td>String(4)</td>
						<td>Y</td>
						<td>Keterangan Daya</td>
					</tr>
					<tr>
						<td>admin_charge</td>
						<td>String()</td>
						<td>Y</td>
						<td>Nominal bea admin</td>
					</tr>
					<tr>
						<td>distribution_code</td>
						<td>String(2)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>service_unit</td>
						<td>String(5)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>service_unit_phone</td>
						<td>String(15)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>max_kwh_unit</td>
						<td>String(5)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>total_repeat</td>
						<td>String(1)</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>power_purchase_unsold</td>
						<td>String()</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>power_purchase_unsold2</td>
						<td>String()</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>saldo</td>
						<td>String()</td>
						<td>Y</td>
						<td>Jumlah sisa saldo, format Number : </td>
					</tr>
					<tr>
						<td>bit11</td>
						<td>String()</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>bit12</td>
						<td>String()</td>
						<td>Y</td>
						<td></td>
					</tr>
					<tr>
						<td>bit48</td>
						<td>String(144)</td>
						<td>Y</td>
						<td>Detail Pelanggan</td>
					</tr>
					<tr>
						<td>bit62</td>
						<td>String(39)</td>
						<td>Y</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>							
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#Request').DataTable({
    	"searching": false,
    	"paging": false,
    	"ordering": false
    });
    $('#Response').DataTable();
} );
</script>