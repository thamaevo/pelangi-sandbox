<div>
	<b>Berikut daftar API Services H2H Messaging :</b>
	<div class="container-fluid">
		<br>
	<div class="row">
		<div class="col-2">
			<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="nav-link active" id="v-pills-Inquiry-tab" data-toggle="pill" href="#v-pills-Inquiry" role="tab" aria-controls="v-pills-Inquiry" aria-selected="true"><b>Inquiry</b></a>
				<a class="nav-link" id="v-pills-Payment-tab" data-toggle="pill" href="#v-pills-Payment" role="tab" aria-controls="v-pills-Payment" aria-selected="true"><b>Payment</b></a>
				<!-- <a class="nav-link" id="v-pills-Purchase-tab" data-toggle="pill" href="#v-pills-Purchase" role="tab" aria-controls="v-pills-Purchase" aria-selected="true">Purchase</a>
				<a class="nav-link" id="v-pills-Advice-tab" data-toggle="pill" href="#v-pills-Advice" role="tab" aria-controls="v-pills-Advice" aria-selected="true">Advice</a>
				<a class="nav-link" id="v-pills-Balance-tab" data-toggle="pill" href="#v-pills-Balance" role="tab" aria-controls="v-pills-Balance" aria-selected="true">Balance Inquiry</a> -->
			</div>
		</div>
		<div class="col-9">

		    <div class="tab-content" id="v-pills-tabContent">
		    	<div class="tab-pane fade show active" id="v-pills-Inquiry" role="tabpanel" aria-labelledby="v-pills-Inquiry-tab">
<p>
	<b>Request :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre class="brush: php">
POST http://202.152.60.61:8118/Transactions/trx.json

trx_date=20200122105548&trx_type=2100&trx_id=5184069732&cust_msisdn=100456789071&cust_account_no=100456789071&product_id=357&product_nomination=&periode_payment=
		</pre>
	</div>
	<p>
	<br>
	<b>Respon JSON :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre class="brush: php">
{
  "data": {
    "trx": {
      "trx_id": "5184069732",
      "stan": "000000351964",
      "datetime": "20200122105548",
      "merchant_code": "6021",
      "bank_code": "4510017",
      "rc": "0000",
      "terminal_id": "0000000000000048",
      "material_number": "12345678901",
      "subscriber_id": "100456789071",
      "pln_refno": "                                ",
      "switcher_refno": "                                ",
      "subscriber_name": "SUBSCRIBER NAME&         ",
      "subscriber_segmentation": "  R1",
      "power": 1300,
      "admin_charge": 0,
      "distribution_code": "  ",
      "service_unit": "     ",
      "service_unit_phone": "               ",
      "max_kwh_unit": "00000",
      "total_repeat": "0",
      "power_purchase_unsold": "",
      "power_purchase_unsold2": "",
      "saldo": "",
      "bit11": "",
      "bit12": "",
      "bit48": "SWID003123456789011004567890711                                                                SUBSCRIBER NAME&           R100000130000000000000",
      "bit62": "                      00000000000000000"
    }
  }
}
	</pre>
</div>
<p>
<br>
	<b>Respon XML :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre class="brush: php">
<?php echo htmlentities('
<xml version="1.0" encoding="utf-8">
<transactions_response>
  <data>
    <trx>
      <trx_id>9572168043</trx_id>
      <stan>000000351750</stan>
      <datetime>20200227143105</datetime>
      <merchant_code>6021</merchant_code>
      <bank_code>4510017</bank_code>
      <rc>0000</rc>
      <terminal_id>0000000000000048</terminal_id>
      <material_number>11000000086</material_number>
      <subscriber_id>100456789071</subscriber_id>
      <pln_refno>                                </pln_refno>
      <switcher_refno>                                </switcher_refno>
      <subscriber_name>SUBSCRIBER NAME&amp;         </subscriber_name>
      <subscriber_segmentation>  R1</subscriber_segmentation>
      <power>1300</power>
      <admin_charge>0</admin_charge>
      <distribution_code>  </distribution_code>
      <service_unit>     </service_unit>
      <service_unit_phone>               </service_unit_phone>
      <max_kwh_unit>00000</max_kwh_unit>
      <total_repeat>0</total_repeat>
      <power_purchase_unsold/>
      <power_purchase_unsold2/>
      <saldo/>
      <bit11/>
      <bit12/>
      <bit48>SWID003110000000861004567890711                                                                SUBSCRIBER NAME&amp;           R100000130000000000000</bit48>
      <bit62>                      00000000000000000</bit62>
    </trx>
  </data>
</transactions_response>');?>

	</pre>
</div>
		    	</div>
		    	<div class="tab-pane fade" id="v-pills-Payment" role="tabpanel" aria-labelledby="v-pills-Payment-tab">
<p>
	<b>Request :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre class="brush: php">
POST http://202.152.60.61:8118/Transactions/trx.json

trx_date=20200122110612&trx_type=2200&trx_id=6308791542&cust_msisdn=100456789071&cust_account_no=100456789071&product_id=357&product_nomination=60000&periode_payment=
		</pre>
	</div>
	<p>
	<br>
	<b>Respon JSON :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre class="brush: php">
{
  "data": {
    "trx": {
      "trx_id": "6308791542",
      "stan": "351977",
      "harga": "60000.0",
      "saldo": "999996282240156",
      "datetime": "20200122110614",
      "merchant_code": "6021",
      "bank_code": "4510017",
      "rc": "0000",
      "terminal_id": "0000000000000048",
      "material_number": "12345678901",
      "subscriber_id": "100456789071",
      "pln_refno": "                                ",
      "switcher_refno": "044121C9BB8D7A44F8FF11204F63CCFE",
      "subscriber_name": "SUBSCRIBER NAME&         ",
      "subscriber_segmentation": "R1  ",
      "power": 1300,
      "admin_charge": 0,
      "distribution_code": "  ",
      "service_unit": "     ",
      "service_unit_phone": "000000000000015",
      "max_kwh_unit": "",
      "total_repeat": "0",
      "token": "12312312311231231231",
      "amount": 20000,
      "angsuran": ".00",
      "power_purchase": "20000.00",
      "jml_kwh": "12.8",
      "ppn": "0.00",
      "ppj": "0.00",
      "meterai": "0.00",
      "power_purchase_unsold": "",
      "power_purchase_unsold2": "",
      "bit11": "",
      "bit12": "",
      "bit48": "SWID003123456789011004567890711                                044121C9BB8D7A44F8FF11204F63CCFE        SUBSCRIBER NAME&         R1       13000200000000002000000000020000000000200000000002000000000020000020000001000000012812312312311231231231",
      "bit62": "       00000000000001500000000000000000",
      "info_text": "RINCIAN TAGIHAN DAPAT DIAKSES& DI \"www.pln.co.id&\" ATAU PLN TERDEKAT"
    }
  }
}
	</pre>
</div>
	<p>
	<br>
	<b>Respon XML :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre class="brush: php">
<?php echo htmlentities('<xml version="1.0" encoding="utf-8">
<transactions_response>
  <data>
    <trx>
      <trx_id>2930547861</trx_id>
      <stan>351752</stan>
      <harga>60080.0</harga>
      <saldo>96967094932</saldo>
      <datetime>20200227144003</datetime>
      <merchant_code>6021</merchant_code>
      <bank_code>4510017</bank_code>
      <rc>0000</rc>
      <terminal_id>0000000000000048</terminal_id>
      <material_number>12345678901</material_number>
      <subscriber_id>100456789071</subscriber_id>
      <pln_refno>                                </pln_refno>
      <switcher_refno>044121C9BB8D7A44F8FF11204F63CCFE</switcher_refno>
      <subscriber_name>SUBSCRIBER NAME&amp;         </subscriber_name>
      <subscriber_segmentation>R1  </subscriber_segmentation>
      <power>1300</power>
      <admin_charge>0</admin_charge>
      <distribution_code>  </distribution_code>
      <service_unit>     </service_unit>
      <service_unit_phone>000000000000015</service_unit_phone>
      <max_kwh_unit/>
      <total_repeat>0</total_repeat>
      <token>12312312311231231231</token>
      <amount>20000</amount>
      <angsuran>.00</angsuran>
      <power_purchase>20000.00</power_purchase>
      <jml_kwh>12.8</jml_kwh>
      <ppn>0.00</ppn>
      <ppj>0.00</ppj>
      <meterai>0.00</meterai>
      <power_purchase_unsold/>
      <power_purchase_unsold2/>
      <bit11/>
      <bit12/>
      <bit48>SWID003123456789011004567890711                                044121C9BB8D7A44F8FF11204F63CCFE        SUBSCRIBER NAME&amp;         R1       13000200000000002000000000020000000000200000000002000000000020000020000001000000012812312312311231231231</bit48>
      <bit62>       00000000000001500000000000000000</bit62>
      <info_text>RINCIAN TAGIHAN DAPAT DIAKSES&amp; DI "www.pln.co.id&amp;" ATAU PLN TERDEKAT</info_text>
    </trx>
  </data>
</transactions_response>');?>
	</pre>
</div>
		    	</div>
		    	<div class="tab-pane fade" id="v-pills-Purchase" role="tabpanel" aria-labelledby="v-pills-Purchase-tab">
<p>
	<b>Request :</b><br>
</p>
	<div class="table table-bordered">
		<pre class="brush: php">
POST http://xxx.xxx.xxx.xxx
Authorization: Basic Base64(client_id:client_secret)
Content-Type: application/json
Origin: [yourdomain.com]

{
	"type":"purchase",
	"merchantType":"6010",
	"agentCode":"00000021",
	"accountType":"H2H",
	"account":"0300000024",
	"institutionCode":"180921000011",
	"product":"3100",
	"billNumber":"08132259999",
	"trxId":"00000004",
	"retrieval":"20180222161223123",
	"sign":"85afd2485c7f91177d006c51ff771f8c"
}
		</pre>
	</div>
	<p>
	<b>Respon :</b><br>
</p>
	<div class="table table-bordered">
		<pre class="brush: php">
{
	"type":"purchase",
	"service":"responsePayment",
	"accountType":"H2H",
	"account":"0300000024",
	"institutionCode":"180921000011",
	"product":"3100",
	"billNumber":"08132259999",
	"resultCode":"000",
	"resultDesc":"SUKSES",
	"nominal":"10050",
	"admin":"0",
	"trxId":"00000004",
	"retrieval":"20180222161223123",
	"paymentCode":"21b555433f8de4b86144642d29d56839",
	"info1":"NO. HANDPHONE : 0081291887774|NILAI VOUCHER : 100000|NO.REF :0041002220476240|",
	"info2":"PEMBELIAN PULSA TELKOMSEL|##Untukkeluhan Hub.133|Npwp TSEL:01.718.327.8.093.000",
	"info3":"",
	"jmlLembar":"1",
	"saldo":"11500000",
	"signature":"85afd2485c7f91177d006c51ff771f8c",
	"merchantType":"6010",
	"agentCode":"00000021"
}
	</pre>
</div>
		    	</div>
		    	<div class="tab-pane fade" id="v-pills-Advice" role="tabpanel" aria-labelledby="v-pills-Advice-tab">
<p>
	<b>Request :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre>
POST http://xxx.xxx.xxx.xxx
Authorization: Basic Base64(client_id:client_secret)
Content-Type: application/json
Origin: [yourdomain.com]

{
	"type":"advice",
	"merchantType":"6010",
	"agentCode":"00000021",
	"accountType":"H2H",
	"account":"0300000024",
	"institutionCode":"180921000011",
	"product":"2042",
	"billNumber":"AF085713323054",
	"trxId":"00000002",
	"retrieval":"20180125003025879",
	"paymentCode":"21b555433f8de4b86144642d29d56839",
	"sign":"e88ab993f7a9ce1a9e2b78e15a485ccf"
}
		</pre>
	</div>
	<p>
	<b>Respon :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre>
{
	"type":"advice",
	"service":"responsePayment",
	"accountType":"H2H",
	"account":"0300000024",
	"institutionCode":"180921000011",
	"product":"2042",
	"billNumber":"AF085713323054",
	"resultCode":"000",
	"resultDesc":"SUKSES",
	"nominal":"304000",
	"admin":"1550",
	"trxId":"00000001",
	"retrieval":"20180125003025879",
	"paymentCode":"21b555433f8de4b86144642d29d56839",
	"info1":"Kode Bayar : AF085713323054|Kode Reff : 900017|ID Transaksi: TKP154821699|Nama : Eduard Budiharto|Desk.Order : Pembelian dari Tokopedia|Jml Tagihan : Rp.304.000|",
	"info2":"TAGIHAN TOKOPEDIA|##1",
	"info3":"",
	"jmlLembar":"1",
	"saldo":"11500000",
	"signature":"e88ab993f7a9ce1a9e2b78e15a485ccf",
	"merchantType":"6010",
	"agentCode":"00000021"
}
	</pre>
</div>
		    	</div>
		    	<div class="tab-pane fade" id="v-pills-Balance" role="tabpanel" aria-labelledby="v-pills-Balance-tab">
<p>
	<b>Request :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre>
POST http://xxx.xxx.xxx.xxx
Authorization: Basic Base64(client_id:client_secret)
Content-Type: application/json
Origin: [yourdomain.com]

{
	"type":"balanceInquiry",
	"merchantType":"6010",
	"agentCode":"00000021",
	"accountType":"H2H",
	"account":"0300000024",
	"institutionCode":"180921000011",
	"product":"CBS",
	"billNumber":"0000000018",
	"trxId":"00000006",
	"retrieval":"20180125003025879",
	"sign":"2855d3b6a6eda783ab5a91b115b59186"
}
		</pre>
	</div>
	<p>
	<b>Respon :</b><br>
</p>
	<div class="bg-white p-4 rounded boderNon">
		<pre>
{
	"type":"balanceInquiry",
	"service":"responseBalanceInquiry",
	"accountType":"H2H",
	"account":"0300000024",
	"product":"CBS",
	"billNumber":"0000000018",
	"resultCode":"00",
	"resultDesc":"success",
	"nominal":"0",
	"admin":"0",
	"trxId":"00000006",
	"retrieval":"20180125003025879",
	"paymentCode":"",
	"info1":"Customer Name : WISNU|Product Code : WADIAH|Balance :2451000|",
	"info2":"01000021",
	"info3":"WISNU|WADIAH|2451000|",
	"jmlLembar":"0",
	"saldo":"2451000",
	"signature":"2855d3b6a6eda783ab5a91b115b59186",
	"merchantType":"6010",
	"agentCode":"00000021"
}
	</pre>
</div>

		    	</div>
		    </div>
		</div>
	</div>							
</div>
</div>