<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>


<div class="container-fluid">
      <div class="col-3">
        <form method="POST" action="<?php echo $this->webroot; ?>daftar_produk">
          <div class="input-group">
            <select class="custom-select" id="produk" name="produk" aria-label="Example select with button addon">
              <option selected>Pilih Produk...</option>
              <option value="100">PLN</option>
              <option value="62">PDAM</option>
              <option value="79">BPJS</option>
              <option value="61">Pulsa Prabayar</option>
              <option value="81">Pulsa Pascabayar</option>
              <option value="43">Telkom</option>
            </select>
            <div class="input-group-append ">
              <button class="btn bg-info text-white" id="btnLoad" type="submit"><b>LOAD</b></button>
            </div>
          </div>
        </form>
  </div>
  <div class="row"><br></div>
	<div class="col-12">
	<table class="table table-bordered boderNon rounded" id="Product" style="width:100%">
		<thead class="thead-dark">
			<!-- <th>No.</th> -->
			<th>Nama</th>
			<th>PRODUK ID</th>
		</thead>
		<tbody class="bg-white">
    		<?php $no = 1; foreach ($products as $product): ?>
			<tr>
				<!-- <td><?php echo $no; ?>&nbsp;</td> -->
				<td><?php echo strtoupper(h($product['0']['name'])); ?>&nbsp;</td>
				<td><?php echo h($product['0']['id']); ?>&nbsp;</td>
			</tr>
			<?php $no++; endforeach; ?>
    	</tbody>
	</table>
	</div>
  <div class="row"><br></div>
</div>
<script type="text/javascript">
  $('#btnLoad').attr('disabled', true);
  $('#produk').change(function() {
      $('#btnLoad').attr('disabled', false);
  });
  $(document).ready(function() {
      $('#Product').DataTable();
  } );
</script>