<b>Errors API Pelangi menggunakan skema error berikut :</b>
<div class="code_syn bg-white p-3 rounded boderNon">
	<pre class="brush: php">
{
  "data": {
    "trx": {
      .... 
	  "rc":"0000",
	  ....
	    }
	}
}
	</pre>
</div>
<br>
<div class="widget-block">
	<div class="widget-head"></div>
	<div class="widget-content">
		<div class="widget-box">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-2 bg-white p-3 rounded boderNon">
						<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						  <a class="nav-link disabled" id="v-pills-plnprepaid-tab" data-toggle="pill" href="#v-pills-plnprepaid" role="tab" aria-controls="v-pills-plnprepaid" aria-selected="true"><b>&nbsp; > LIST PRODUK <</b></a>
						  <a class="nav-link active" id="v-pills-plnprepaid-tab" data-toggle="pill" href="#v-pills-plnprepaid" role="tab" aria-controls="v-pills-plnprepaid" aria-selected="true"><b>PLN PREPAID</b></a>
						  <a class="nav-link" id="v-pills-plnpostpaid-tab" data-toggle="pill" href="#v-pills-plnpostpaid" role="tab" aria-controls="v-pills-plnpostpaid" aria-selected="true"><b>PLN POSTPAID</b></a>
						  <a class="nav-link" id="v-pills-plnnontaglis-tab" data-toggle="pill" href="#v-pills-plnnontaglis" role="tab" aria-controls="v-pills-plnnontaglis" aria-selected="true"><b>PLN NONTAGLIS</b></a>
						  <a class="nav-link" id="v-pills-pdam-tab" data-toggle="pill" href="#v-pills-pdam" role="tab" aria-controls="v-pills-pdam" aria-selected="false"><b>PDAM</b></a>
						  <a class="nav-link" id="v-pills-bpjs-tab" data-toggle="pill" href="#v-pills-bpjs" role="tab" aria-controls="v-pills-bpjs" aria-selected="false"><b>BPJS</b></a>
						  <a class="nav-link" id="v-pills-telkom-tab" data-toggle="pill" href="#v-pills-telkom" role="tab" aria-controls="v-pills-telkom" aria-selected="false"><b>TELKOM</b></a>
						  <a class="nav-link" id="v-pills-telkommini-tab" data-toggle="pill" href="#v-pills-telkommini" role="tab" aria-controls="v-pills-telkommini" aria-selected="false"><b>TELKOM MINIPACK</b></a>
						  <a class="nav-link" id="v-pills-pulsapra-tab" data-toggle="pill" href="#v-pills-pulsapra" role="tab" aria-controls="v-pills-pulsapra" aria-selected="false"><b>PULSA PRABAYAR</b></a>
						  <a class="nav-link" id="v-pills-pulsapasca-tab" data-toggle="pill" href="#v-pills-pulsapasca" role="tab" aria-controls="v-pills-pulsapasca" aria-selected="false"><b>PULSA PASCABAYAR</b></a>
						  <a class="nav-link" id="v-pills-tv-tab" data-toggle="pill" href="#v-pills-tv" role="tab" aria-controls="v-pills-tv" aria-selected="false"><b>TV KABEL</b></a>
						  <a class="nav-link" id="v-pills-game-tab" data-toggle="pill" href="#v-pills-game" role="tab" aria-controls="v-pills-game" aria-selected="false"><b>VOUCHER GAME</b></a>
						  <a class="nav-link" id="v-pills-mf-tab" data-toggle="pill" href="#v-pills-mf" role="tab" aria-controls="v-pills-mf" aria-selected="false"><b>MULTIFINANCE</b></a>
						</div>
					</div>
					<div class="col-md-10 p-5 rounded">
						<div class="tab-content" id="v-pills-tabContent">
							<div class="tab-pane fade show active" id="v-pills-plnprepaid" role="tabpanel" aria-labelledby="v-pills-plnprepaid-tab">
								<div class="text-center"><b>MAPPING RC PRODUK PLN PREPAID</b></div>
								<hr>
								<div class="widget-box">
									<table class="table table-striped table-bordered rounded boderNon" id="MapPLNprepaid">
										<thead class="thead-dark">
											<tr>
												<th>Code Response</th>
												<th>Message Response</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0006</td>
												<td>ERROR-blocked partner central</td>
											</tr>
											<tr>
												<td>0007</td>
												<td>ERROR-blocked terminal</td>
											</tr>
											<tr>
												<td>0013</td>
												<td>ERROR-Invalid Transaction Amount</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0015</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0017</td>
												<td>ERROR-Subscriber still have bills to pay</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Format Salah</td>
											</tr>
											<tr>
												<td>0031</td>
												<td>ERROR-Unregistered Bank Code</td>
											</tr>
											<tr>
												<td>0032</td>
												<td>ERROR-Unregistered Switching</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0034</td>
												<td>ERROR-Unregistered Terminal</td>
											</tr>
											<tr>
												<td>0040</td>
												<td>ERROR-Stok kosong</td>
											</tr>
											<tr>
												<td>0041</td>
												<td>ERROR-Transaction Amount below minimum purchase amount</td>
											</tr>
											<tr>
												<td>0042</td>
												<td>ERROR-Transaction Amount exceed maximum purchase amount</td>
											</tr>
											<tr>
												<td>0045</td>
												<td>ERROR-Invalid admin charge</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0047</td>
												<td>ERROR-Total KWH is over the limit</td>
											</tr>
											<tr>
												<td>0063</td>
												<td>ERROR-No Payment</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Time out</td>
											</tr>
											<tr>
												<td>0077</td>
												<td>ERROR-Subscriber suspended</td>
											</tr>
											<tr>
												<td>0088</td>
												<td>ERROR-Bills already paid</td>
											</tr>
											<tr>
												<td>0089</td>
												<td>ERROR-Current bill is not available</td>
											</tr>
											<tr>
												<td>0090</td>
												<td>ERROR-Cut-off is in progress</td>
											</tr>
											<tr>
												<td>0092</td>
												<td>ERROR-Invalid Reference Number</td>
											</tr>
											<tr>
												<td>0093</td>
												<td>ERROR-Invalid Partner Central Trace Audit Number</td>
											</tr>
											<tr>
												<td>0094</td>
												<td>ERROR-Reversal had been done</td>
											</tr>
											<tr>
												<td>0097</td>
												<td>ERROR-Switching ID and / or Bank Code is not identical with payment</td>
											</tr>
											<tr>
												<td>0098</td>
												<td>ERROR-PLN Ref Number is not valid</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="v-pills-plnpostpaid" role="tabpanel" aria-labelledby="v-pills-plnpostpaid-tab">
								<div class="text-center"><b>MAPPING RC PRODUK PLN POSTPAID</b></div>
								<hr>
								<div class="widget-box">
									<table class="table table-striped table-bordered rounded boderNon" id="MapPLNpostpaid">
										<thead class="thead-dark">
											<tr>
												<th>Code Response</th>
												<th>Message Response</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0006</td>
												<td>ERROR-blocked partner central</td>
											</tr>
											<tr>
												<td>0007</td>
												<td>ERROR-blocked terminal</td>
											</tr>
											<tr>
												<td>0013</td>
												<td>ERROR-Invalid Transaction Amount</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0015</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0017</td>
												<td>ERROR-Subscriber still have bills to pay</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Format Salah</td>
											</tr>
											<tr>
												<td>0031</td>
												<td>ERROR-Unregistered Bank Code</td>
											</tr>
											<tr>
												<td>0032</td>
												<td>ERROR-Unregistered Switching</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0034</td>
												<td>ERROR-Unregistered Terminal</td>
											</tr>
											<tr>
												<td>0040</td>
												<td>ERROR-Stok kosong</td>
											</tr>
											<tr>
												<td>0041</td>
												<td>ERROR-Transaction Amount below minimum purchase amount</td>
											</tr>
											<tr>
												<td>0042</td>
												<td>ERROR-Transaction Amount exceed maximum purchase amount</td>
											</tr>
											<tr>
												<td>0045</td>
												<td>ERROR-Invalid admin charge</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0047</td>
												<td>ERROR-Total KWH is over the limit</td>
											</tr>
											<tr>
												<td>0063</td>
												<td>ERROR-No Payment</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Time out</td>
											</tr>
											<tr>
												<td>0077</td>
												<td>ERROR-Subscriber suspended</td>
											</tr>
											<tr>
												<td>0088</td>
												<td>ERROR-Bills already paid</td>
											</tr>
											<tr>
												<td>0089</td>
												<td>ERROR-Current bill is not available</td>
											</tr>
											<tr>
												<td>0090</td>
												<td>ERROR-Cut-off is in progress</td>
											</tr>
											<tr>
												<td>0092</td>
												<td>ERROR-Invalid Reference Number</td>
											</tr>
											<tr>
												<td>0093</td>
												<td>ERROR-Invalid Partner Central Trace Audit Number</td>
											</tr>
											<tr>
												<td>0094</td>
												<td>ERROR-Reversal had been done</td>
											</tr>
											<tr>
												<td>0097</td>
												<td>ERROR-Switching ID and / or Bank Code is not identical with payment</td>
											</tr>
											<tr>
												<td>0098</td>
												<td>ERROR-PLN Ref Number is not valid</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="v-pills-plnnontaglis" role="tabpanel" aria-labelledby="v-pills-plnnontaglis-tab">
								<div class="text-center"><b>MAPPING RC PRODUK PLN NONTAGLIS</b></div>
								<hr>
								<div class="widget-box">
									<table class="table table-striped table-bordered rounded boderNon" id="MapPLNnontaglis">
										<thead class="thead-dark">
											<tr>
												<th>Code Response</th>
												<th>Message Response</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0006</td>
												<td>ERROR-blocked partner central</td>
											</tr>
											<tr>
												<td>0007</td>
												<td>ERROR-blocked terminal</td>
											</tr>
											<tr>
												<td>0013</td>
												<td>ERROR-Invalid Transaction Amount</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0015</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0017</td>
												<td>ERROR-Subscriber still have bills to pay</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Format Salah</td>
											</tr>
											<tr>
												<td>0031</td>
												<td>ERROR-Unregistered Bank Code</td>
											</tr>
											<tr>
												<td>0032</td>
												<td>ERROR-Unregistered Switching</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0034</td>
												<td>ERROR-Unregistered Terminal</td>
											</tr>
											<tr>
												<td>0040</td>
												<td>ERROR-Stok kosong</td>
											</tr>
											<tr>
												<td>0041</td>
												<td>ERROR-Transaction Amount below minimum purchase amount</td>
											</tr>
											<tr>
												<td>0042</td>
												<td>ERROR-Transaction Amount exceed maximum purchase amount</td>
											</tr>
											<tr>
												<td>0045</td>
												<td>ERROR-Invalid admin charge</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0047</td>
												<td>ERROR-Total KWH is over the limit</td>
											</tr>
											<tr>
												<td>0063</td>
												<td>ERROR-No Payment</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Time out</td>
											</tr>
											<tr>
												<td>0077</td>
												<td>ERROR-Subscriber suspended</td>
											</tr>
											<tr>
												<td>0088</td>
												<td>ERROR-Bills already paid</td>
											</tr>
											<tr>
												<td>0089</td>
												<td>ERROR-Current bill is not available</td>
											</tr>
											<tr>
												<td>0090</td>
												<td>ERROR-Cut-off is in progress</td>
											</tr>
											<tr>
												<td>0092</td>
												<td>ERROR-Invalid Reference Number</td>
											</tr>
											<tr>
												<td>0093</td>
												<td>ERROR-Invalid Partner Central Trace Audit Number</td>
											</tr>
											<tr>
												<td>0094</td>
												<td>ERROR-Reversal had been done</td>
											</tr>
											<tr>
												<td>0097</td>
												<td>ERROR-Switching ID and / or Bank Code is not identical with payment</td>
											</tr>
											<tr>
												<td>0098</td>
												<td>ERROR-PLN Ref Number is not valid</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="v-pills-pdam" role="tabpanel" aria-labelledby="v-pills-pdam-tab">
								<div class="text-center"><b>MAPPING RC PRODUK PDAM</b></div>
								<hr>
								<div class="widget-box">
									<table class="table table-striped table-bordered rounded boderNon" id="MapPdam">
										<thead class="thead-dark">
											<tr>
												<th>Code Response</th>
												<th>Message Response</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Invalid format</td>
											</tr>
											<tr>
												<td>0040</td>
												<td>ERROR-Empty stock</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0012</td>
												<td>ERROR-Reversal exceeded time limit</td>
											</tr>
											<tr>
												<td>0013</td>
												<td>ERROR-Invalid transaction amount</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-Invalid customer number</td>
											</tr>
											<tr>
												<td>0015</td>
												<td>ERROR-Invalid customer number</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered product</td>
											</tr>
											<tr>
												<td>0034</td>
												<td>ERROR-Bills already paid</td>
											</tr>
											<tr>
												<td>0045</td>
												<td>ERROR-Invalid admin fee</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient deposit</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Time out</td>
											</tr>
											<tr>
												<td>0090</td>
												<td>ERROR-Bills already paid</td>
											</tr>
											<tr>
												<td>0092</td>
												<td>ERROR-Current bill is not available</td>
											</tr>
											<tr>
												<td>0093</td>
												<td>ERROR-No payment</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="v-pills-bpjs" role="tabpanel" aria-labelledby="v-pills-bpjs-tab">
								<div class="text-center"><b>MAPPING RC PRODUK BPJS</b></div>
								<hr>
								<div class="widget-box">
									<table class="table table-striped table-bordered rounded boderNon" id="MapBpjs">
										<thead class="thead-dark">
											<tr>
												<th>Code Response</th>
												<th>Message Response</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0006</td>
												<td>ERROR-blocked partner central</td>
											</tr>
											<tr>
												<td>0007</td>
												<td>ERROR-blocked terminal</td>
											</tr>
											<tr>
												<td>0008</td>
												<td>ERROR-Invalid-access time</td>
											</tr>
											<tr>
												<td>0011</td>
												<td>ERROR-Need to sign-on</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>NOMOR VIRTUAL ACCOUNT SALAH, MOHON TELITI KEMBALI</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Invalid message</td>
											</tr>
											<tr>
												<td>0031</td>
												<td>ERROR-Unregristered BankCode</td>
											</tr>
											<tr>
												<td>0032</td>
												<td>ERROR-Unregristered Partner Central</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0034</td>
												<td>IURAN SUDAH TERBAYAR</td>
											</tr>
											<tr>
												<td>0051</td>
												<td>Periode Tidak Valid</td>
											</tr>
											<tr>
												<td>0063</td>
												<td>ERROR-No Payment</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Timeout</td>
											</tr>
											<tr>
												<td>0088</td>
												<td>IURAN SUDAH TERBAYAR</td>
											</tr>
											<tr>
												<td>0089</td>
												<td>ERROR-Current bill is not available</td>
											</tr>
											<tr>
												<td>0090</td>
												<td>ERROR-Cut-off is in progress</td>
											</tr>
											<tr>
												<td>0004</td>
												<td>ERROR-Unregistered bills</td>
											</tr>
											<tr>
												<td>0045</td>
												<td>RROR-Invalid admin Charges</td>
											</tr>
											<tr>
												<td>0069</td>
												<td>ERROR-Timeout and payment have been reversal by switching</td>
											</tr>
											<tr>
												<td>0088</td>
												<td>IURAN SUDAH TERBAYAR</td>
											</tr>
											<tr>
												<td>0090</td>
												<td>ERROR-Cut-off is in progress</td>
											</tr>
											<tr>
												<td>0092</td>
												<td>ERROR-Switcher ReferenceNumber is not available</td>
											</tr>
											<tr>
												<td>0093</td>
												<td>ERROR-Invalid Switcher Trace Audit Number</td>
											</tr>
											<tr>
												<td>0097</td>
												<td>ERROR-Switching ID and/or BankCode is not identical with inquiry</td>
											</tr>
											<tr>
												<td>0098</td>
												<td>ERROR Reference Number is not valid</td>
											</tr>
											<tr>
												<td>0035</td>
												<td>ERROR-Unregristered terminal</td>
											</tr>
											<tr>
												<td>0043</td>
												<td>Blocked / expired phone number</td>
											</tr>
											<tr>
												<td>0044</td>
												<td>voucher out of stock</td>
											</tr>
											<tr>
												<td>0405</td>
												<td>Virtual account number tidak sesuai</td>
											</tr>
											<tr>
												<td>0047</td>
												<td>Invalid date</td>
											</tr>
											<tr>
												<td>0048</td>
												<td>MAKSIMAL PEMBAYARAN 12 BULAN</td>
											</tr>
											<tr>
												<td>0049</td>
												<td>No Virtual Account tidak terdafta</td>
											</tr>
											<tr>
												<td>0050</td>
												<td>Gagal aktifasi</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>SALDO TIDAK MENCUKUPI</td>
											</tr>
											<tr>
												<td>0076</td>
												<td>ERROR LAINNYA</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="v-pills-telkom" role="tabpanel" aria-labelledby="v-pills-telkom-tab">
								<div class="text-center"><b>MAPPING RC PRODUK TELKOM</b></div>
								<hr>
								<div class="widget-box">
									<table class="table table-striped table-bordered rounded boderNon" id="MapTelkom">
										<thead class="thead-dark">
											<tr>
												<th>Code Response</th>
												<th>Message Response</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-ID Pel Salah</td>
											</tr>
											<tr>
												<td>0015</td>
												<td>ERROR-ID Pel Salah</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0045</td>
												<td>ERROR-Invalid Admin charge</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Time out</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="v-pills-telkommini" role="tabpanel" aria-labelledby="v-pills-telkommini-tab">
								<div class="text-center"><b>MAPPING RC PRODUK TELKOM MINIPACK</b></div>
								<hr>
								<div class="widget-box">
									<table class="table table-striped table-bordered rounded boderNon" id="MapTelkommini">
										<thead class="thead-dark">
											<tr>
												<th>Code Response</th>
												<th>Message Response</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0006</td>
												<td>ERROR-blocked partner central</td>
											</tr>
											<tr>
												<td>0007</td>
												<td>ERROR-blocked terminal</td>
											</tr>
											<tr>
												<td>0013</td>
												<td>ERROR-Invalid Transaction Amount</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0015</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Format Salah</td>
											</tr>
											<tr>
												<td>0031</td>
												<td>ERROR-Unregristered Bank Code</td>
											</tr>
											<tr>
												<td>0032</td>
												<td>ERROR-Unregistered Switching</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0034</td>
												<td>ERROR-Unregistered Terminal</td>
											</tr>
											<tr>
												<td>0040</td>
												<td>ERROR-Stok kosong</td>
											</tr>
											<tr>
												<td>0041</td>
												<td>ERROR-Transaction Amount below minimumpurchase amount</td>
											</tr>
											<tr>
												<td>0042</td>
												<td>ERROR-Transaction Amount exceed maximumpurchase amount</td>
											</tr>
											<tr>
												<td>0045</td>
												<td>ERROR-Invalid admin charge</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0063</td>
												<td>ERROR-No Payment</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Time out</td>
											</tr>
											<tr>
												<td>0077</td>
												<td>ERROR-Subscriber suspended</td>
											</tr>
											<tr>
												<td>0088</td>
												<td>ERROR-Bills already paid</td>
											</tr>
											<tr>
												<td>0089</td>
												<td> ERROR-Current bill is not available</td>
											</tr>
											<tr>
												<td>0090</td>
												<td>ERROR-Cut-off is in progress</td>
											</tr>
											<tr>
												<td>0092</td>
												<td>ERROR-Invalid Reference Number</td>
											</tr>
											<tr>
												<td>0093</td>
												<td>ERROR-Invalid Partner Central Trace AuditNumber</td>
											</tr>
											<tr>
												<td>0094</td>
												<td>ERROR-Reversal had been done</td>
											</tr>
											<tr>
												<td>0097</td>
												<td>ERROR-Switching ID and / or Bank Code is not identical with payment</td>
											</tr>
											<tr>
												<td>0098</td>
												<td>ERROR-Ref Number is not valid</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="v-pills-pulsapra" role="tabpanel" aria-labelledby="v-pills-pulsapra-tab">
								<div class="text-center"><b>MAPPING RC PRODUK PULSA PRABAYAR</b></div>
								<hr>
									<table class="table table-striped table-bordered rounded boderNon" id="MapPulsapra">
										<thead class="thead-dark">
											<tr>
												<th>Kode Respon</th>
												<th>Pesan Respon</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Format Salah</td>
											</tr>
											<tr>
												<td>0040</td>
												<td>ERROR-Stok kosong</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0013</td>
												<td>ERROR-Invalid transaction amount</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-MSISDN Salah</td>
											</tr>
											<tr>
												<td>0015</td>
												<td>ERROR-MSISDN Salah</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0045</td>
												<td>ERROR-Invalid Admin charge</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Time out</td>
											</tr>
										</tbody>
									</table>
							</div>
							<div class="tab-pane fade" id="v-pills-pulsapasca" role="tabpanel" aria-labelledby="v-pills-pulsapasca-tab">
								<div class="text-center"><b>MAPPING RC PRODUK PULSA PASCABAYAR</b></div>
								<hr>
									<table class="table table-striped table-bordered rounded boderNon" id="MapPulsapasca">
										<thead class="thead-dark">
											<tr>
												<th>Kode Respon</th>
												<th>Pesan Respon</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Format Salah</td>
											</tr>
											<tr>
												<td>0040</td>
												<td>ERROR-Stok kosong</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0013</td>
												<td>ERROR-Invalid transaction amount</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-MSISDN Salah</td>
											</tr>
											<tr>
												<td>0015</td>
												<td>ERROR-MSISDN Salah</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0045</td>
												<td>ERROR-Invalid Admin charge</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Time out</td>
											</tr>
										</tbody>
									</table>
							</div>
							<div class="tab-pane fade" id="v-pills-tv" role="tabpanel" aria-labelledby="v-pills-tv-tab">
								<div class="text-center"><b>MAPPING RC PRODUK TV KABEL</b></div>
								<hr>
									<table class="table table-striped table-bordered rounded boderNon" id="MapTv">
										<thead class="thead-dark">
											<tr>
												<th>Kode Respon</th>
												<th>Pesan Respon</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Format Salah</td>
											</tr>
											<tr>
												<td>0006</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0018</td>
												<td>ERROR-Reversal melebihi batas waktu</td>
											</tr>
											<tr>
												<td>0013</td>
												<td>ERROR-Invalid transaction amount</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0018</td>
												<td> ERROR-Time out</td>
											</tr>
											<tr>
												<td>0034</td>
												<td> ERROR-Bills already paid</td>
											</tr>
											<tr>
												<td>0034</td>
												<td>ERROR-Current bill is not available</td>
											</tr>
										</tbody>
									</table>
							</div>
							<div class="tab-pane fade" id="v-pills-game" role="tabpanel" aria-labelledby="v-pills-game-tab">
								<div class="text-center"><b>MAPPING RC PRODUK VOUCHER GAME</b></div>
								<hr>
								<div class="widget-box">
									<table class="table table-striped table-bordered rounded boderNon" id="MapGame">
										<thead class="thead-dark">
											<tr>
												<th>Kode Respon</th>
												<th>Pesan Respon</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Format Salah</td>
											</tr>
											<tr>
												<td>0040</td>
												<td>ERROR-Stok kosong</td>
											</tr>
											<tr>
												<td>0005</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0068</td>
												<td>ERROR-Time out</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="v-pills-mf" role="tabpanel" aria-labelledby="v-pills-mf-tab">
								<div class="text-center"><b>MAPPING RC PRODUK MULTINANCE</b></div>
								<hr>
									<table class="table table-striped table-bordered rounded boderNon" id="MapMf">
										<thead class="thead-dark">
											<tr>
												<th>Kode Respon</th>
												<th>Pesan Respon</th>
											</tr>
										</thead>
										<tbody class="bg-white">
											<tr>
												<td width="20%">0000</td>
												<td>SUCCESS</td>
											</tr>
											<tr>
												<td>0001</td>
												<td>PENDING</td>
											</tr>
											<tr>
												<td>0030</td>
												<td>ERROR-Format Salah</td>
											</tr>
											<tr>
												<td>0006</td>
												<td>ERROR-Other</td>
											</tr>
											<tr>
												<td>0018</td>
												<td>ERROR-Reversal melebihi batas waktu</td>
											</tr>
											<tr>
												<td>0013</td>
												<td>ERROR-Invalid transaction amount</td>
											</tr>
											<tr>
												<td>0014</td>
												<td>ERROR-ID Pelanggan Salah</td>
											</tr>
											<tr>
												<td>0033</td>
												<td>ERROR-Unregistered Product</td>
											</tr>
											<tr>
												<td>0046</td>
												<td>ERROR-Insufficient Deposit</td>
											</tr>
											<tr>
												<td>0018</td>
												<td> ERROR-Time out</td>
											</tr>
											<tr>
												<td>0034</td>
												<td> ERROR-Bills already paid</td>
											</tr>
											<tr>
												<td>0034</td>
												<td>ERROR-Current bill is not available</td>
											</tr>
										</tbody>
									</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#MapPLNprepaid').DataTable();
    $('#MapPLNpostpaid').DataTable();
    $('#MapPLNnontaglis').DataTable();
    $('#MapPdam').DataTable();
    $('#MapBpjs').DataTable();
    $('#MapTelkom').DataTable();
    $('#MapTelkommini').DataTable();
    $('#MapPulsapra').DataTable();
    $('#MapPulsapasca').DataTable();
    $('#MapTv').DataTable();
    $('#MapGame').DataTable();
    $('#MapMf').DataTable();
} );
</script>