<div class="">
<p>
	<b>Berikut Overview dari Pelangi Indodata Sandbox APIs:</b>
<br>
<table class="table table-bordered boderNon">
	<thead class="thead-dark">
		<tr>
			<th>
				Method
			</th>
			<th>
				EndPoint
			</th>
			<th>
				Usage
			</th>
		</tr>
	</thead>
	<tbody class="bg-white">
		<tr>
			<td>
				POST
			</td>
			<td>
				http://202.152.60.61:8118/Transactions/trx.json
			</td>
			<td>
				API
			</td>
		</tr>
	</tbody>
</table>
Silahkan coba APIs kami menggunakan Sandbox. Kami menyediakan sandbox dengan data dummy dan static.
</p>
</div>