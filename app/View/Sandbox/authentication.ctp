<div class="">
<p><b>HTTP Method</b>
</p>
<ul>
	<li>
		HTTP Method yang digunakan adalah POST.
	</li>
	<li>
		HTTP Method harus dikirim dengan huruf capital.
	</li>
</ul>
<p>
<b>Headers</b><br>
Agar komunikasi berhasil dengan APIs Pelangi, anda harus mengirim beberapa headers di setiap request API :
</p>
<table class="table table-bordered boderNon rounded">
	<thead class="thead-dark">
		<tr>
			<th>
				Name
			</th>
			<th>
				Type
			</th>
			<th>
				Description
			</th>
		</tr>
	</thead>
	<tbody class="bg-white">
		<tr>
			<td>
				Authorization
			</td>
			<td>
				Alphanumeric
			</td>
			<td>
				Basic Base64(client_id:client_secret)
			</td>
		</tr>
		<tr>
			<td>
				Content-Type
			</td>
			<td>
				Alphanumeric
			</td>
			<td>
				(application/json)
			</td>
		</tr>
		<tr>
			<td>
				Origin
			</td>
			<td>
				Url
			</td>
			<td>
				(Origin domain e.g. yourdomain.com)
			</td>
		</tr>
	</tbody>
</table>
<p>
Signature digunakan oleh APIs Pelangi untuk menjamin bahwa pesan yang ada kirim tidak diubah oleh yang tidak berhak. Garis besar proses pembuatan Signature adalah sebagai berikut:<br>
</p>
<ol>
	<li>&emsp; Ambil parameter $pel_user APIs yang diberikan oleh Pelangi (ex : plg12110004).</li>
	<li>&emsp; Ambil parameter $pel_pass APIs yang diberikan oleh Pelangi (ex : aldwin.3).</li>
	<li>&emsp; Ambil parameter $signature APIs yang diberikan oleh Pelangi (ex : abcd1234).</li>
	<li>&emsp; Gabungkan semua parameter diatas kemudian disusun menjadi <br>
		&emsp; Authorization: PELANGIREST username='.$pel_user.'&password='.$pel_pass.'&signature='.$signature</li>
	<li>&emsp; Kirim parameter tersebut ke URL APIs yang diberikan oleh Pelangi (ex : http://202.152.60.61:8118/Transactions/trx.json).</li>
</ol>
<p>

<b>Body</b><br>
<p>
Body berisikan parameter untuk melakukan transaksi ke Pelangi. Garis besar proses pembuatan Body adalah sebagai berikut:<br>

</p>
<ol>
	<li>&emsp; Ambil nilai dari field trx_date (ex : 20200122105546).</li>
	<li>&emsp; Ambil nilai dari field trx_type (ex : 2100 untuk Inquiry, 2200 untuk Payment).</li>
	<li>&emsp; Ambil nilai dari field trx_id (ex : 5184069732).</li>
	<li>&emsp; Ambil nilai dari field cust_msisdn (ex : 100456789071).</li>
	<li>&emsp; Ambil nilai dari field cust_account_no (ex : 100456789071).</li>
	<li>&emsp; Ambil nilai dari field product_id (ex : 357).</li>
	<li>&emsp; Ambil nilai dari field product_nomination (ex : 0).</li>
	<li>&emsp; Ambil nilai dari field periode_payment (ex : 0).</li>
</ol>
Contoh Message :
<br>

	<pre class="brush: php">
	trx_date=20200219114752&trx_type=2100&trx_id=6107549832&cust_msisdn=021055500001&cust_account_no=&product_id=102&product_nomination=&periode_payment=
	</pre>

</p>
</div>