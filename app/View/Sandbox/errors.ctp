<b>ErrorsAPI PosFin menggunakan skema error berikut :</b>
<div class="code_syn">
	<pre class="prettyprint linenums">
{ 
	"resultCode":"00",
	"resultDesc":"success"
}
	</pre>
</div>
<div class="widget-block">
	<div class="widget-head"></div>
	<div class="widget-content">
		<div class="widget-box">
			<table class="table table-striped table-bordered">
				<thead class="thead-dark">
					<tr>
						<th>Kode Respon</th>
						<th>Pesan Respon</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>000</td>
						<td>Sukses</td>
					</tr>
					<tr>
						<td>301</td>
						<td>Parameter kosong (tidak valid)</td>
					</tr>
					<tr>
						<td>102</td>
						<td>Mohon maaf, System dalam maintenance. Mohon coba kembali 3 menit kedepan</td>
					</tr>
					<tr>
						<td>103</td>
						<td>Mohon maaf, Transaksi tidak bisa dilakukan di atas cutt of time</td>
					</tr>
					<tr>
						<td>104</td>
						<td>Kode Produk tidak valid</td>
					</tr>
					<tr>
						<td>105</td>
						<td>Mohon maaf, Transaksi tidak bisa dilakukan di atas cutt of time</td>
					</tr>
					<tr>
						<td>106</td>
						<td>Mandatory field tidak valid (tidak boleh kosong)</td>
					</tr>
					<tr>
						<td>107</td>
						<td>Panjang trxId tidak valid</td>
					</tr>
					<tr>
						<td>108</td>
						<td>Content message dari database kosong</td>
					</tr>
					<tr>
						<td>109</td>
						<td>Gagal proses database</td>
					</tr>
					<tr>
						<td>198</td>
						<td>Gagal proses message</td>
					</tr>
					<tr>
						<td>998</td>
						<td>Mohon maaf, service B2B sedang maintenance</td>
					</tr>
					<tr>
						<td>997</td>
						<td>Parameter tidak valid</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>Request Inquiry / Purchase</td>
					</tr>
					<tr>
						<td>996</td>
						<td>Kode institution tidak valid atau tidak ditemukan</td>
					</tr>
					<tr>
						<td>995</td>
						<td>Produk tidak valid atau tidak aktif</td>
					</tr>
					<tr>
						<td>994</td>
						<td>Signature is not match</td>
					</tr>
					<tr>
						<td>993</td>
						<td>Service (Produk) belum memiliki routing host</td>
					</tr>
					<tr>
						<td>992</td>
						<td>Account anda terblokir. Mohon hubungi adminstrator</td>
					</tr>
					<tr>
						<td>991</td>
						<td>Member belum memiliki harga referensi untuk produk ini</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td>Request Payment</td>
					</tr>
					<tr>
						<td>996</td>
						<td>paymentCode tidak ditemukan (reqPayment)</td>
					</tr>
					<tr>
						<td>995</td>
						<td>paymentCode ini sudah di-payment. Lakukan cek status (reqPayment)</td>
					</tr>
					<tr>
						<td>994</td>
						<td>Signature is not match (reqPay)</td>
					</tr>
					<tr>
						<td>993</td>
						<td>Service (Produk) belum memiliki routing host (reqPay)</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>