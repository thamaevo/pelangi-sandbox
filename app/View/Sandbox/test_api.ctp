<p>
<b>Test API</b>
</p>
<div class="container">
	<div class="row" >
		<div class="col border p-1 mb-2 rounded bg-white boderNon">
			<div class="border p-1 mb-2 bg-dark text-white rounded">
				<b>Parameter Request</b>
			</div>
			<div class="col border p-1 mb-2 rounded">
				<form id="request">
					<div class="form-group row">
						<label class="col-sm-5 col-form-label"><b>trx_date</b> : </label>
						<div class="col-sm-7">
							<input type="text" class="form-control form-control-sm boderNon" name="trx_date" value="<?php date_default_timezone_set("Asia/Jakarta"); echo date('YmdHis'); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-5 col-form-label"><b>trx_type</b> : </label>
						<div class="col-sm-7">
							<input type="text" class="form-control form-control-sm boderNon" name="trx_type" value="2100">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-5 col-form-label"><b>trx_id</b> : </label>
						<div class="col-sm-7">
							<input type="text" class="form-control form-control-sm boderNon" name="trx_id" value="<?php echo substr(str_shuffle("0123456789"), 0, 10); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-5 col-form-label"><b>cust_msisdn</b> : </label>
						<div class="col-sm-7">
							<input type="text" class="form-control form-control-sm boderNon" name="cust_msisdn" value="100456789071">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-5 col-form-label"><b>cust_account_no</b> : </label>
						<div class="col-sm-7">
							<input type="text" class="form-control form-control-sm boderNon" name="cust_account_no" value="100456789071">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-5 col-form-label"><b>product_id</b> : </label>
						<div class="col-sm-7">
							<input type="text" class="form-control form-control-sm boderNon" name="product_id" value="357">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-5 col-form-label"><b>product_nomination</b> : </label>
						<div class="col-sm-7">
							<input type="text" class="form-control form-control-sm boderNon" name="product_nomination" value="0">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-5 col-form-label"><b>periode_payment</b> : </label>
						<div class="col-sm-7">
							<input type="text" class="form-control form-control-sm boderNon" name="periode_payment" value="0">
						</div>
					</div>
					<button type="submit" class="btn btn-primary mb-2"><b>SEND</b></button>
				</form>
			</div>
		</div>
		&nbsp;
		&nbsp;
		<div class="col-7 border p-1 mb-2 rounded  bg-white boderNon">
			<div class="border p-1 mb-2 bg-dark text-white rounded">
				<b>Request</b>
			</div>
			<div class="col border p-1 mb-2 rounded">
				<div class="boderNon">
					<div id="requestsource" class="text-break"  style="word-wrap: break-word;">

					</div>
				</div>
			</div>
			<div class="border p-1 mb-2 bg-dark text-white rounded">
				<b>Response</b>
			</div>
			<div class="col border p-1 mb-2 rounded">
				<div class="boderNon">
					<div id="responsesource" class="text-break" style="word-wrap: break-word;">

					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$('#request').submit(function(event) {
		event.preventDefault()
		$('#responsesource').html('loading')
          var requestpost = getFormData($('#request').serializeArray())
          console.log(requestpost)
          $('#requestsource').html('<pre> trx_date='+requestpost.trx_date+'&trx_type='+requestpost.trx_type+'&trx_id='+requestpost.trx_id+'&cust_msisdn='+requestpost.cust_msisdn+'&cust_account_no='+requestpost.cust_account_no+'&product_id='+requestpost.product_id+'&product_nomination='+requestpost.product_nomination+'&periode_payment='+requestpost.periode_payment+' </pre>')

          $.ajax({
        type: "POST",
        data:requestpost,
        url: "/sandbox/test_api_response",
        success : function(response){
          console.log(response)
          $('#responsesource').html('<pre>'+response+'</pre>')
        }
      })

        });
		var getFormData = function(formData) {
        var dataJson = {};
        $.each(formData, function(index, val) {
          dataJson[val.name] = val.value;
        });
        return dataJson;
      }
</script>