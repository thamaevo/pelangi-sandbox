<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <title>PELANGI SANDBOX</title>
    <?php
		echo $this->Html->meta('icon');
	?>
        <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
<style>
	.boderNav{
		border: 2px solid black;
		border-radius: 7px;
	}
	.boderNon{
		border: 1px solid black;
		box-shadow: 2px 3px;
		border-radius: 5px;
	}
	.boderNon2{
		border: 1px solid black;
		box-shadow: 2px 2px 2px 2px;
	}
	.center {
	  margin: auto;
	  padding: 10px;
	}
</style>

</head>
<body>
	<div class="col-md-11 center">
		<div id="header">
			<center>
				<br>
				<div>
  					<img src="<?php echo $this->webroot; ?>img/logoo_pelangi.png" class="img-fluid" alt="Responsive image" >
  				</div>
				<br>
			</center>
		</div>
		<div id="content">
			<ul class="nav nav-pills nav-fill bg-white boderNav">
			  <li class="nav-item">
			  	<a class="nav-link <?php if ($this->request->url == null) {echo "active text-white";}else{echo "text-dark";}; ?> m-2" href="<?php echo $this->Html->url('/');?>"><b>Overview</b></a>
			  </li>
			  <li class="nav-item">
			  	<a class="nav-link <?php if ($this->request->url == "authentication") {echo "active text-white";}else{echo "text-dark";}; ?> m-2" href="authentication"><b>Authentication</b></a>
			  </li>
			  <li class="nav-item">
			  	<a class="nav-link <?php if ($this->request->url == "api_parameter") {echo "active text-white";}else{echo "text-dark";}; ?> m-2" href="api_parameter"><b>API Parameter</b></a>
			  </li>
			  <li class="nav-item">
			  	<a class="nav-link <?php if ($this->request->url == "api_services") {echo "active text-white";}else{echo "text-dark";}; ?> m-2" href="api_services"><b>API Services</b></a>
			  </li>
			  <li class="nav-item">
			  	<a class="nav-link <?php if ($this->request->url == "rc") {echo "active text-white";}else{echo "text-dark";}; ?> m-2" href="rc"><b>Response Code</b></a>
			  </li>
			  <li class="nav-item">
			  	<a class="nav-link <?php if ($this->request->url == "daftar_produk") {echo "active text-white";}else{echo "text-dark";}; ?> m-2" href="daftar_produk"><b>Product List</b></a>
			  </li>
			  <li class="nav-item">
			  	<a class="nav-link <?php if ($this->request->url == "test_api") {echo "active text-white";}else{echo "text-dark";}; ?> m-2" href="test_api"><b>Test API</b></a>
			  </li>
			  <li class="nav-item">
			  	<a class="nav-link <?php if ($this->request->url == "download") {echo "active text-white";}else{echo "text-dark";}; ?> m-2" href="download"><b>Download</b></a>
			  </li>
			  <li class="nav-item">
			  	<a class="nav-link <?php if ($this->request->url == "contact") {echo "active text-white";}else{echo "text-dark";}; ?> m-2" href="contact"><b>Contact</b></a>
			  </li>
			</ul>
		

			<div class="mb-2"></div>
			<div class="container-fluid rounded" style="background-color:#f6f4e6;border: 2px solid black">
				<br>
					<?php echo $this->Flash->render(); ?>
					<?php echo $this->fetch('content'); ?>
					<br>
					<br>
			</div>
		</div>

		<div id="footer">
	      <div class="clearfix">
	      	<div class="container-md">
				<div class="pull-right" align="center">
					<div style="height: 2rem" align="center"></div>
					<div style="height: 5rem" align="center">
						@ Copyright 2020 
						<b>Pelangi Indodata</b>
					</div>
	        	</div>
	      	</div>
	      </div>
		</div>
	</div>
</body>
</html>
